/**
 * @file CFG.cpp
 * @class mnb::CFG
 * @class mnb::CFGNode
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *
 *  Created on: Nov 15, 2013
 */

#include "CFG.h"
#include "../CONV/CONV.h"

namespace mnb {

/**
 * @brief constructs a CFG
 * @post the object must be properly initialized
 */
CFG::CFG(): InitCheck(this){
	_verbose = _verbose->instance(false);
	REQUIRE(properlyInitialized(),
			"Object not well defined: CFG");
}

/**
 * @brief constructs a CFG from a given XML-file adds a verbose mode to the functions
 * @param string fileName, path and name of XML-file to parse
 * @param bool verbose, gives additional output for debugging, standard set to false.
 * @post the object must be properly initialized.
 */
CFG::CFG(std::string fileName, bool verb) : InitCheck(this){
	_verbose = _verbose->instance(verb);
	_verbose->addFunction("CFG::CFG(std::string fileName, bool verb)");
	this->parse(fileName);
	REQUIRE(properlyInitialized(),
			"Object not well defined: CFG");
	_verbose->closeCurrentFunction();
}

/**
 * @brief default destructor for class CFG
 */
CFG::~CFG(){}

/**
 * @brief Checks CFGness
 * @return whether or not the given Grammar is a CFG
 * @pre Object must be properly initialized
 * @pre Object may not be empty
 */
bool CFG::isCFG(){
	_verbose->addFunction("CFG::isCFG()");

	// pre condition
	REQUIRE(properlyInitialized(), "CFG could not be parsed, not properly initialized");
	REQUIRE(notEmpty(), "CFG is empty");

	// needs for a CFG:
	//	- none of the fields is empty
	//	- Startsymbol inside of Variables
	_verbose->comment("Verifying startsymbol");
	if (_V.find(_S) == _V.end()){
		std::string comment = "Startsymbol";
		comment+=_S;
		comment += " is not in variables";
		_verbose->comment(comment);
		return false;
	}
	_verbose->comment("Startsymbol part of variables");


	int maxstringlength = 0;
	_verbose->comment("searching maximum string length in _V and _T");
	for(std::set< std::string >::iterator it = _V.begin(); it != _V.end(); it++){
		if((*it).size() > maxstringlength){
			maxstringlength = (*it).size();
			std::string comment = "New maxstringlength found:";
			comment += convertInt(maxstringlength);
			_verbose->comment(comment);
		}
	}
	for(std::set< std::string >::iterator it = _T.begin(); it != _T.end(); it++){
		if((*it).size() > maxstringlength){
			maxstringlength = (*it).size();
			std::string comment = "New maxstringlength found:";
			comment += convertInt(maxstringlength);
			_verbose->comment(comment);
		}
	}
	_verbose->comment("verifying all productions on using variables and terminals only");
	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
			it != _P.end(); ++it){
		for (int i =0; i<=1; i++){
			std::string t = "";
			if(i==0) t = it->first;
			else  t = it->second;

			std::string temp = t;
			std::string check = "";
			for(int i=0; i<temp.size(); i++){
				for(int j=i; (j<=i+maxstringlength) && (j<=temp.size()); j++){
					std::string substring = temp.substr(i,j);
					if((_V.find(substring) != _V.end()) || (_T.find(substring) != _T.end())){
						check += substring;
						std::string comment = "String ";
						comment += substring;
						comment += " found in _V or _T";
						_verbose->comment(comment);
					}
					if (check == temp)break;
				}
				if (check == temp)break;
			}
			if(check != temp){
				std::string comment = "Could not find all subparts of string: ";
				comment += temp;
				comment += ", instead found: ";
				comment += check;
				_verbose->comment(comment);
				return false;
			}
		}
	}
	_verbose->comment("all productios are valid");
	_verbose->closeCurrentFunction();
	return true;
}

/**
 * @brief Prints the CFG to the terminal
 * @pre Object must be properly initialized
 */
void CFG::print(){
	_verbose->addFunction("CFG::print()");
	// pre condition
	REQUIRE(properlyInitialized(),
			"CFG could not be printed, not properly initialized");

	std::cout<<"====================CFG======================="
			<<std::endl;

	//Print out the variables===
	_verbose->comment("printing variables");
	std::cout<<"Variables = {";
	std::set<std::string>::iterator it = _V.begin();
	std::cout<<*it;
	++it;
	for (;it !=_V.end(); ++it){
		std::cout<<", ";
		std::cout<<*it;
	}
	std::cout<<"}"<<std::endl;
	//print out the terminals
	_verbose->comment("printing terminals");
	std::cout<<"Terminals = {";
	std::set<std::string>::iterator it2 = _T.begin();
	std::cout<<*it2;
	it2++;
	for (; it2 !=_T.end(); ++it2){
		std::cout<<", ";
		std::cout<<*it2;
	}
	std::cout<<"}"<<std::endl;
	//print out startsymbol
	_verbose->comment("printing startsymbol");
	std::cout<<"Startsymbol = {"<<_S<<"}"<<std::endl;

	//print out productions
	_verbose->comment("printing productions");
	std::cout<<"Production = {";
	std::set<std::pair<std::string, std::string> >::iterator it3 = _P.begin();
	std::cout<<"("<<it3->first<<" => "<<it3->second<<")";
	it3++;
	for (; it3 !=_P.end(); ++it3){
		std::cout<<std::endl;
		std::cout<<"("<<it3->first<<" => "<<it3->second<<") ";

	}
	std::cout<<"}"<<std::endl;
	std::cout<<"=============================================="<<std::endl;
	_verbose->closeCurrentFunction();
}

/**
 * @brief Prints the given CFG to a XML-file
 * @param string fileName the given name of the XML representation of the GFG
 * @param string path the location where the XML-file should be written.
 * 				standard set to standard location.
 * @pre Object must be properly initialized
 * @post file must exist
 */
void CFG::printToXML(std::string fileName, std::string path){
	_verbose->addFunction("CFG::printToXML(std::string fileName, std::string path)");

	REQUIRE(properlyInitialized(),
			"CFG could not be writen, not properly initialized");
	//create new XML doc
	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();

	//create root nodes
	_verbose->comment("creating all container nodes");
	tinyxml2::XMLNode* cfg = doc->InsertEndChild( doc->NewElement( "CFG" ) );

	//insert all variables
	_verbose->comment("[1/5] CFG-root node created");
	tinyxml2::XMLNode* var = cfg->InsertFirstChild(doc->NewElement( "Variables" )) ;

	for (std::set<std::string>::iterator it = _V.begin(); it !=_V.end(); ++it){
		_verbose->comment("Adding variable subnode V");
		tinyxml2::XMLNode* v = var->InsertEndChild(doc->NewElement( "V" ));
		const char * text = (*it).c_str();
		v->InsertFirstChild( doc->NewText( text));
	}
	_verbose->comment("[2/5] Variable node created");

	//insert startsymbol
	tinyxml2::XMLNode* ssl = cfg->InsertAfterChild(var, doc->NewElement( "StartSymbol" )) ;
	const char * text = (_S).c_str();
	ssl->InsertFirstChild( doc->NewText( text));
	_verbose->comment("[3/5] StartSymbol node created");

	//insert terminals
	tinyxml2::XMLNode* ter = cfg->InsertAfterChild(ssl,doc->NewElement( "Terminals" )) ;
	for (std::set<std::string>::iterator it = _T.begin(); it !=_T.end(); ++it){
		_verbose->comment("Adding terminal subnode T");
		tinyxml2::XMLNode* t = ter->InsertEndChild(doc->NewElement( "T" ));
		const char *text = (*it).c_str();
		t->InsertFirstChild( doc->NewText( text));
	}

	_verbose->comment("[4/5] Terminals node created");

	//insert Productions
	tinyxml2::XMLNode* prd = cfg->InsertAfterChild(ter, doc->NewElement( "Productions" )) ;
	for (std::set<std::pair<std::string, std::string> >::iterator it = _P.begin(); it !=_P.end(); ++it){
		_verbose->comment("Adding production subnode P");
		tinyxml2::XMLNode* p = prd->InsertEndChild(doc->NewElement( "P" ));
		tinyxml2::XMLNode* lp= p->InsertFirstChild(doc->NewElement( "lP" ));
		tinyxml2::XMLNode* rp= p->InsertEndChild(doc->NewElement( "rP" ));

		const char *text = it->first.c_str();
		lp->InsertFirstChild( doc->NewText( text));

		const char *text2 = it->second.c_str();
		rp->InsertFirstChild( doc->NewText( text2));

	}
	_verbose->comment("[5/5] Productions node created");

	doc->SaveFile(fileName.c_str());
	ENSURE(doc->fileExists(fileName),
			"File could not be created");
	_verbose->closeCurrentFunction();
}

/**
 * @brief Checks whether a string belongs to the language
 * @param string a, string that wil be checked on being in the language of the CFG
 * @pre Object must be properly initialized
 */
bool CFG::stringInGrammar(std::string s){
	_verbose->addFunction("CFG::stringInGrammar");
	REQUIRE(properlyInitialized(),
			"Object was not well defined : CFG");

	// utilizes cfg for testing.
	CONV* conv = new CONV(_verbose->isVerbose());
	PDA cfg = conv->CFGtoPDA(*this);
	bool toreturn = cfg.simulate(s);
	delete conv;
	_verbose->closeCurrentFunction();
	return toreturn;
}

/**
 * @brief returns private member _P, all productions of the CFG
 * @return map<string, string> P
 */
const std::set<std::pair<std::string, std::string> >& CFG::getP() const {
	return _P;
}


/**
 * @brief get the startsymbol of the CFG
 * @return string S
 */
const std::string& CFG::getS() const {
	return _S;
}

/**
 * @brief gets all the terminals of the CFG
 * @return set<char> T
 */
const std::set<std::string>& CFG::getT() const {
	return _T;
}

/**
 * @brief gets all the variables of the CFG
 * @return set<string> V
 */
const std::set<std::string>& CFG::getV() const {
	return _V;
}


/**
 * @brief checks if the class is properly initialized
 * @return true if properly initialized
 */
bool CFG::properlyInitialized(){
	return (this == InitCheck);
}

/**
 * @brief sets the productions
 * @param const std::set<std::pair<std::string, std::string> >& p set of productions
 */
void CFG::setP(const std::set<std::pair<std::string, std::string> >& p) {
	_P = p;
}

/**
 * @brief sets the startsymbol
 * @param const std::string& s the startsymbol
 */
void CFG::setS(const std::string& s) {
	_S = s;
}

/**
 * @brief sets the terminals
 * @param std::set<std::string>& t set of terminals
 */
void CFG::setT(const std::set<std::string>& t) {
	_T = t;
}

/**
 * @brief sets the variables
 * @param const std::set<std::string>& v the variables
 */
void CFG::setV(const std::set<std::string>& v) {
	_V = v;
}

/**
 * @brief checks if a string s is present in the variables
 * @return true if string is in variables
 * @pre object must be properly initialized
 */
bool CFG::inVariables(std::string s){
	REQUIRE(this->properlyInitialized(),
			"Object not well defined : CFG");
	if (s.size()>1)return false;
	return (_V.find(s) != _V.end());
}

/**
 * @brief parse the XML so that it becomes a CFG
 * @param std::string fileName the file that must be parsed
 * @pre object must be properly initialized
 * @pre the file is valid XML file and a valid path
 * @post none of the variables of the CFG can be empty
 */
void CFG::parse(std::string& fileName){
	_verbose->addFunction("CFG::parse(std::string& fileName)");
	REQUIRE(this->properlyInitialized(),
			"Object not well defined : CFG");
	try{
		tinyxml2::XMLDocument doc;
		//check if file exists
		REQUIRE(doc.fileExists(fileName), "file does not exists");
		//load xml file
		doc.LoadFile(fileName.c_str());

		//temp data for correct verbose output
		std::string elementName;
		std::string message;

		//search the root node
		tinyxml2::XMLNode *rootNode = doc.FirstChild();
		if(!rootNode) throw "There was no root node found.";
		elementName = std::string(rootNode->Value());
		message = "Found the root node: <" + elementName + ">";
		_verbose->comment(message);

		//search the sibling after the root
		tinyxml2::XMLNode* siblingNode = rootNode ->NextSibling();
		if(!siblingNode) throw "there is no siblingNode for the rootNode";
		elementName = std::string(siblingNode->Value());
		message = "Found the sibling node: <" + elementName + ">";
		_verbose->comment(message);

		//get the first child of the PDA element
		tinyxml2::XMLNode* childNode = siblingNode ->FirstChild();
		if(!childNode) throw "there is no childNode for <CFG>";

		//loop over all the children of the CFG element, as long as there are siblings
		while(childNode){
			elementName = std::string(childNode->Value());
			message = "found element " + elementName;
			_verbose->comment(message);
			//check if child of the pda element has children of his own
			//if yes than we need to add multiple elements else we need to add just one
			if(childNode->FirstChild()->FirstChild() == NULL){
				readElement(childNode);
			}
			else{
				readElements(childNode);
			}
			//go to the next child of the pda element
			childNode = childNode->NextSibling();
		}

	}catch(const char* err){
		_verbose->error(err);
		_verbose->closeCurrentFunction();

	}
	ENSURE(notEmpty(), "Could not parse, a field remained empty");
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to read a element with multiple children.
 * @param tinyxml2::XMLNode* root the root of the current location in the XML.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @throw "Encountered a unrecognizable element";
 *
 */
void CFG::readElements(tinyxml2::XMLNode* root){
	_verbose->addFunction("PDA::readElements(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "elements could not be parsed, not properly initialized");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";
	//get the name of the current element
	std::string elementName = root->Value();
	//pre-initialized data so that it does not need to be initialized in the while loop
	tinyxml2::XMLNode* childNode = root->FirstChild();
	tinyxml2::XMLElement *childElmtl;
	std::string data;
	std::string message;
	//loop over the child elements of the current element, as long as there are siblings
	while(childNode){
		//if it is the transitions value then we need to call a extra function, otherwise we can handle it here
		if(elementName != "Productions"){
			childElmtl = childNode->ToElement();
			//the data of this element
			data = std::string(childElmtl->GetText());
			//add the the data to the object variable
			if(elementName == "Variables"){
				message = "Add " + data + " to States";
				_verbose->comment(message);
				_V.insert(data);
			}
			else if(elementName == "Terminals"){
				message = "Add " + data + " to AcceptingStates";
				_verbose->comment(message);
				_T.insert(data);
			}
			else{
				throw "Encountered a unrecognizable element";
			}
		}
		else{
			readProductions(childNode);
		}
		//get the next sibling of the current element
		childNode = childNode->NextSibling();
	}
	_verbose->closeCurrentFunction();

}

/**
 * @brief a function to read a element with just one child.
 * @param tinyxml2::XMLNode* root the root of the current location in the XML.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @throw "Encountered a unrecognizable element";
 */
void CFG::readElement(tinyxml2::XMLNode* root){
	_verbose->addFunction("CFG::readElement(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "element could not be parsed, not properly initialized");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";
	//get the name of the current element
	std::string elementName = root->Value();
	//get the first child of the current element
	tinyxml2::XMLNode* childNode = root->FirstChild();
	//temp data for correct verbose output
	std::string data;
	std::string message;
	//the data of this element
	data = std::string(childNode->Value());
	//add the the data to the object variable
	if(elementName == "StartSymbol"){
		message = "Add " + data + " to S";
		_verbose->comment(message);
		_S = data;
	}
	else{
		throw "Encountered a unrecognizable element";
	}
	_verbose->closeCurrentFunction();
}

/**
 * @biref a function to read the Productions elements
 * @param tinyxml2::XMLNode* root the root of the current location in the XML.
 * @pre object must be properly initialized
 * @throw root pointer is null
 */
void CFG::readProductions(tinyxml2::XMLNode* root){
	//verbose begin function
	_verbose->addFunction("CFG::readProductions(tinyxml2::XMLNode* root)");
	// pre condition
	REQUIRE(this->properlyInitialized(), "CFG could not be parsed, not properly initialized");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";

	//pre-initialized data
	tinyxml2::XMLNode* childNode = root->FirstChild();
	tinyxml2::XMLNode* childSiblNode;
	tinyxml2::XMLElement *childElmtl;
	tinyxml2::XMLElement *childSiblElmtl;
	//temp data for correct verbose output
	std::string data;
	std::string data2;
	std::string message;
	//set all the necessary data
	childElmtl = childNode->ToElement();
	data = std::string(childElmtl->GetText());
	childSiblNode = childNode->NextSibling();
	childSiblElmtl = childSiblNode->ToElement();
	data2 = std::string(childSiblElmtl->GetText());
	message = "Add (" + data + "," + data2 + ") to Production";
	_verbose->comment(message);
	//add the the data to the object variable
	_P.insert(std::make_pair(data, data2));
	_verbose->closeCurrentFunction();
}

/**
 * helper function for class CFG
 * check all datamembers on emptyness
 * @return bool whether or not one of the members is empty
 * @pre object must be properly initialized
 */
bool CFG::notEmpty(){
	//verbose begin function
	_verbose->addFunction("CFG::notEmpty()");
	// pre condition
	REQUIRE(this->properlyInitialized(), "CFG could not be parsed, not properly initialized");

	if(_V.empty()){
		_verbose->error("V is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_T.empty()){
		_verbose->error("T is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_P.empty()){
		_verbose->error("P is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_S.empty()){
		_verbose->error("S is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else {
		_verbose->comment("Nothing is empty");
		_verbose->closeCurrentFunction();
		return true;
	}
}

/**
 * @brief function converts a cfg to chomsky normal form
 * @pre object must be properly initialized
 * @see properlyInitialized()
 */
void CFG::Chomsky(){
	_verbose->addFunction("CFG::Chomsky()");
	REQUIRE(this->properlyInitialized(),
			"object not well defined : CFG");
	// eliminate useless symbols
	// find all usefull symbols
		//Start by taking the startsymbol
	std::set<std::string> usefullsymbols;

	// find all reachable children
	std::set<std::string> found;
	CFGNode* start = new CFGNode(_S, _verbose->isVerbose());
	found.insert(_S);
	std::stack<CFGNode* > fringe;
	fringe.push(start);
	while (!fringe.empty()){
		CFGNode* n = fringe.top();
		fringe.pop();
		std::vector<std::string> children1 = getProductions(n->getV());
		std::set<std::string> children = stripToVarTer(children1);
		for (std::set<std::string>::iterator c = children.begin();
				c != children.end(); c++){
			if (found.find(*c) == found.end()){
				std::string comment = "adding variable ";
				comment += *c;
				comment += " to the fringe";
				_verbose->comment(comment);
				found.insert(*c);
				CFGNode* node = new CFGNode(*c, _verbose->isVerbose());
				fringe.push(node);
				n->addchild(node);
			}
		}
	}
	// find all usefull symbols
	fringe.push(start);
	while (!fringe.empty()){
		CFGNode* n = fringe.top();
		fringe.pop();
		if(n->is_usefull(_T) && inVariables(n->getV())){
			usefullsymbols.insert(n->getV());
		}
		std::vector<CFGNode*> temp = n->getChildren();
		for(std::vector<CFGNode*>::iterator it = temp.begin();
				it != temp.end(); it++){
			fringe.push((*it));
		}
	}
	// delete all useless symbols
	for (std::set< std::string >::iterator it = _V.begin(); it != _V.end(); it++){
		if(usefullsymbols.find((*it)) == usefullsymbols.end()){
			_V.erase(it);
		}
	}

	// eliminate unit productions
	//find all unit productions
	std::queue<std::pair<std::string, std::string> > up;

	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
			it != _P.end(); it++){
		if(inVariables(it->first) && inVariables(it->second)){
			up.push((*it));
		}
	}
	// add equivalent productions
	while(!up.empty()){
		std::pair<std::string, std::string> n = up.front();
		up.pop();

		std::set<std::pair<std::string, std::string> > prod = findProd(n.second);
		for(std::set<std::pair<std::string, std::string> >::iterator it = prod.begin();
					it != prod.end(); it++){
			_P.insert(make_pair(n.first, it->second));
			if (inVariables(n.first) && inVariables(it->second)){
				up.push( make_pair(n.first, it->second) );
			}
		}
	}
	// delete unit productions
	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
			it != _P.end(); it++){
		if(inVariables(it->first) && inVariables(it->second)){
			_P.erase((*it));
		}
	}

	// find all productions with a second part that is greater than 2 symbols
	std::queue<std::pair<std::string, std::string> > tb;
	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
				it != _P.end(); it++){
		if(it->second.size() > 2){
			tb.push((*it));
		}
	}
	while(!tb.empty()){
		std::pair<std::string, std::string> n = tb.front();
		tb.pop();
		int oversize = n.second.size()-2;
		std::vector<std::string> newVar;

		// find a new ascii char that we did not yet use
		char asciichar = 65; //A
		std::stringstream ss;
		for (int i = 0; i< oversize; i++){
			while(true){
				ss<<asciichar;
				std::string test = ss.str();
				ss.str(std::string());

				if (inVariables(test) || inVector(newVar, test)){
					asciichar++;
				}else{
					break;
				}
			}
			ss<<asciichar;
			newVar.push_back(ss.str());
			_V.insert(ss.str());
			asciichar++;
		}
		std::string newpr = n.second.at(0) + newVar.at(0);
		_P.insert(std::make_pair(n.first, newpr));
		for(int i=1; i <oversize; i++){
			newpr = n.second.at(i) + newVar.at(i);
			_P.insert(std::make_pair(newVar.at(i-1), newpr));
		}
		newpr = n.second.substr(n.second.size()-2);
		_P.insert(std::make_pair(newVar.at(newVar.size()-1), newpr));
	}
	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
				it != _P.end(); it++){
		if(it->second.size() > 2){
			_P.erase((*it));
		}
	}
}

/**
 * @brief helper function, returns all production for a given string s
 * @param string s
 * @return all production related to string s
 * @pre object must be properly initialized
 */
std::vector<std::string> CFG::getProductions(std::string s){
	_verbose->addFunction("CFG::getProductions(std::string s)");
	REQUIRE(this->properlyInitialized(),
			"object not well defined : CFG");
	std::string comment = "finding productions for string: ";
	comment += s;
	_verbose->comment(comment);

	std::vector<std::string> to_return;

	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
				it != _P.end(); ++it){
		if(it->first == s) to_return.push_back(it->second);
	}
	comment = convertInt(to_return.size());
	comment += " productions found";
	_verbose->comment(comment);
	_verbose->closeCurrentFunction();
	return to_return;
}

/**
 * @brief strips a string to variables and terminals
 * @param string to parse
 * @return set of all found variables and terminals
 * @pre object must be properly initialized
 */
std::set<std::string> CFG::stripToVarTer(std::vector<std::string> s){
	_verbose->addFunction("CFG::stripToVar(std::vector<std::string> s)");
	REQUIRE(this->properlyInitialized(),
			"object not well defined : CFG");
	std::set<std::string> to_return;
	std::string comment;
	std::stringstream ss;
	for (std::vector<std::string>::iterator it = s.begin(); it != s.end(); it++){
		comment = "stripping ";
		comment += (*it);
		_verbose->comment(comment);
		for(int i = 0; i<(*it).size(); i++){
			ss<<(*it).at(i);
			std::string substring = ss.str();
			ss.str(std::string());
			if (_V.find(substring) != _V.end()){
				to_return.insert(substring);
			}
			if (_T.find(substring) != _T.end()){
				to_return.insert(substring);
			}
		}
	}
	comment = convertInt(to_return.size());
	comment += " variables and terminals found";
	_verbose->comment(comment);
	_verbose->closeCurrentFunction();
	return to_return;
}

/**
 * @brief checks if a given string s is present inside the terminals
 * @param string s, string to search
 * @return true if string is in terminals
 * @pre object must be properly initialized
 */
bool CFG::inTerminals(std::string s){
	REQUIRE(this->properlyInitialized(),
			"object not well defined : CFG");
	for(int i=0; i<s.size(); i++){
		if ( _T.find(s.substr(i, i+1)) == _T.end()) return false;
	}
	return true;
}


/**
 * @brief finds a production for a string s
 * @param string s, string to find productions for
 * @return set of all possible productions
 * @pre object must be well defined
 */
std::set<std::pair<std::string, std::string> > CFG::findProd(std::string s){
	_verbose->addFunction("CFG::findProd(std::string s)");
	std::set<std::pair<std::string, std::string> > to_return;

	for(std::set<std::pair<std::string, std::string> >::iterator it = _P.begin();
			it != _P.end(); it++){
		if (it->first == s){
			to_return.insert((*it));
		}
	}
	_verbose->closeCurrentFunction();
	return to_return;
}


/**
 * @brief checks wether or not a cfg node is usefull
 * @param set of all terminals
 * @return true if node is usefull
 */
bool CFGNode::is_usefull(std::set< std::string > t){
	if(usefull)return usefull;
	else if(t.find(V)!= t.end()) usefull = true;
	else{
		for (std::vector<CFGNode*>::iterator c = children.begin();
				c != children.end(); c++){
			if((*c)->is_usefull(t)) usefull = true;
		}
	}
	return usefull;
}



} /* namespace mnb */
