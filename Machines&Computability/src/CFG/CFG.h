/**
 * @file CFG.h
 * @class mnb::CFG
 * @brief Class for all CFG simulation
 *
 * @class mnb::CFGNode
 * @brief helper class for CFG
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *
 *  Created on: Nov 15, 2013
 */


#pragma once
#ifndef CFG_H_
#define CFG_H_

#include <set>
#include <map>
#include <string>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <queue>
#include "../Verbose/Verbose.h"
#include "../DesignByContract.h"
#include "../Parser/tinyxml2.h"



namespace mnb {

class CFG{
public:
	CFG();
	CFG(std::string fileName, bool verbose = false);
	virtual ~CFG();

	bool isCFG();
	void print();
	void printToXML(std::string fileName, std::string path = "");
	bool stringInGrammar(std::string a);

	const std::set<std::pair<std::string, std::string> >& getP() const;
	const std::string& getS() const;
	const std::set<std::string>& getT() const;
	const std::set<std::string>& getV() const;

	void setP(const std::set<std::pair<std::string, std::string> >& p);
	void setS(const std::string& s);
	void setT(const std::set<std::string>& t);
	void setV(const std::set<std::string>& v);

	bool properlyInitialized();
	bool inVariables(std::string s);

	std::set<std::pair<std::string, std::string> > findProd(std::string s);
	void Chomsky();

private:
	//privat e functions
	void parse(std::string& fileName);
	void readElements(tinyxml2::XMLNode* root);
	void readElement(tinyxml2::XMLNode* root);
	void readProductions(tinyxml2::XMLNode* root);

	bool notEmpty();
	bool inTerminals(std::string s);
	std::vector<std::string> getProductions(std::string s);
	std::set<std::string> stripToVarTer(std::vector<std::string> s);

	//private members
	Verbose* _verbose;
	CFG* InitCheck;

	std::set< std::string > _V;
	std::set< std::string > _T;
	std::set<std::pair<std::string, std::string> > _P;
	std::string _S;
};

inline std::string convertInt(int number)
{
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

inline bool inVector(std::vector<std::string> v, std::string s){
	for (std::vector<std::string>::iterator it = v.begin();
			it != v.end(); it++){
		if ((*it) == s) return true;
	}
	return false;

}

class CFGNode{
public:
	CFGNode(std::string v = "", bool verb = false):
		V(v),
		_verbose(_verbose->instance(verb)),
		usefull(false){};

	void setV (std::string v){
		V = v;
	}
	std::string getV(){
		return V;
	}
	void addchild(CFGNode* child){
		children.push_back(child);
	}
	std::vector<CFGNode*> getChildren(){
		return children;
	}
	bool is_usefull(std::set< std::string > t);

private:
	Verbose* _verbose;
	std::string V;
	std::vector<CFGNode*> children;
	bool usefull;

};

} /* namespace mnb */
#endif /* CFG_H_ */
