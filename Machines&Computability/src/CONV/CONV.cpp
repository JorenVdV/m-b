/**
 * @file CONV.cpp
 * @class mnb::CONV
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Nov 15, 2013
 */


#include "CONV.h"

namespace mnb {

/**
 * @brief the constructor for the converter classes.
 * @post object must be properly initialized
 * @see properlyInitialized()
 */
CONV::CONV(bool verb): _verbose(_verbose->instance(verb)), InitCheck(this) {
	ENSURE(this->properlyInitialized(), "the converter is not properlyInitialized");
}

/**
 * @brief standard destructor for the converter classe;
 */
CONV::~CONV() {
	// TODO Auto-generated destructor stub
}

/**
 * @brief a function that converts a given CFG to a PDA
 * @param CFG cfg the CFG that need to be converted
 * @return PDA the pda that is been created
 * @pre object must be properly initialized
 * @post the CFG should be a real CFG
 * @see properlyInitialized()
 * @see isCFG()
 */
CFG CONV::PDAtoCFG(PDA& pda){
	REQUIRE(this->properlyInitialized(), "the converter is not properlyInitialized");
	_verbose->addFunction("CONV::CFGtoPDA(CFG cfg)");
	CFG cfg;

	_verbose->comment("[0/3] set the terminals");
	std::set<std::string> E = pda.getE();
	std::set<std::string> T;
	for(std::set<std::string>::iterator it = E.begin(); it != E.end(); ++it){
		T.insert((*it));
	}
	cfg.setT(T);

	_verbose->comment("[1/3] create the variable");
	std::set<std::string> states = pda.getQ();
	std::set<std::string> stackAlphabet = pda.getS();
	std::string variable;
	std::set<std::string> V;
	for(std::set<std::string>::iterator s_it = stackAlphabet.begin(); s_it != stackAlphabet.end(); ++s_it){
		for(std::set<std::string>::iterator it = states.begin(); it != states.end(); ++it){
			for(std::set<std::string>::iterator _it = states.begin(); _it != states.end(); ++_it){
				variable = (*it) + (*s_it) + (*_it);
				V.insert(variable);
			}
		}
	}
	V.insert("S");
	cfg.setV(V);

	_verbose->comment("[2/3] set the start state");
	cfg.setS("S");

	_verbose->comment("[3/3] create the productions");
	std::vector<std::pair<Triple<std::string>, Tuple<std::string> > > transitions = pda.getT();
	std::set<std::pair<std::string, std::string> > productions;
	std::set<std::pair<std::string, std::string> > _productions;
	Triple<std::string> leftOperant;
	Tuple<std::string> rightOperant;

	productions = createStartProduction(states, pda.getQ0(), pda.getZ0());

	for(unsigned int i = 0; i < transitions.size(); ++i){
		leftOperant = transitions.at(i).first;
		rightOperant = transitions.at(i).second;
		_productions = createProductions(leftOperant.first(), rightOperant.first(), states, leftOperant.thirth(), rightOperant.second(), leftOperant.second());
		productions.insert(_productions.begin(), _productions.end());
	}
	cfg.setP(productions);


	_verbose->comment("CFG is created");
	_verbose->closeCurrentFunction();
	// TODO: make the ensure working -------> ENSURE(cfg.isCFG(), "the CFG that is created is not a CFG");
	return cfg;
}
/**
	 * @brief a function that converts a given PDA to a CFG
	 * @param PDA pda the pda that need to be converted
	 * @return CFG the cfg that is been created
	 * @pre object must be properly initialized
	 * @post the PDA should be a real PDA
	 * @see properlyInitialized()
	 * @see isPDA()
	 */
PDA CONV::CFGtoPDA(CFG& cfg){
	REQUIRE(this->properlyInitialized(), "the converter is not properlyInitialized");
	_verbose->addFunction("CONV::CFGtoPDA(CFG cfg)");
	PDA pda;
	//set the initial state
	_verbose->comment("[0/6] set the initial state");
	pda.setQ0("q0");
	_verbose->comment("[1/6] set the possible states");
	std::set<std::string> Q;
	//set the possible states
	Q.insert("q0");
	Q.insert("q1");
	Q.insert("q2");
	pda.setQ(Q);
	///set the accepting state
	_verbose->comment("[2/6] set the accepting state");
	std::set<std::string> F;
	F.insert("q2");
	pda.setF(F);


	std::set<std::string> variable;
	std::set<std::string> stackAlphabet;

	_verbose->comment("[3/6] set the input alphabet");
	std::set<std::string> terminals;
	terminals = cfg.getT();
	pda.setE(terminals);

	_verbose->comment("[4/6] set the start stack symbol");
	pda.setZ0("@");

	_verbose->comment("[5/6] set the stack alphabet");
	variable = cfg.getV();
	stackAlphabet = terminals;
	stackAlphabet.insert(pda.getZ0());
	for(std::set<std::string>::iterator it = variable.begin(); it != variable.end(); ++it){
		stackAlphabet.insert((*it));
	}
	pda.setS(stackAlphabet);


	_verbose->comment("[6/6] create transitions");
	std::set<std::pair<std::string, std::string> > P = cfg.getP();

	std::vector<std::pair<Triple<std::string>, Tuple<std::string> > > T;

	Triple<std::string> lt = Triple<std::string>(pda.getQ0(), "e", pda.getZ0());
	std::string temp = cfg.getS() + pda.getZ0();
	Tuple<std::string> rt ;/*= Tuple<std::string>("q1", temp);*/
	rt.add("q1");
	rt.add(temp);
	T.push_back(std::make_pair(lt,rt));

	lt =  Triple<std::string>("q1", "e", pda.getZ0());
	//rt = Tuple<std::string>("q2", pda.getZ0());
	rt.add("q2");
	rt.add(pda.getZ0());

	T.push_back(std::make_pair(lt,rt));

	for(std::set<std::pair<std::string, std::string> >::iterator it = P.begin(); it != P.end(); ++it){
		lt = Triple<std::string>("q1", "e", (*it).first);
		//rt = Tuple<std::string>("q1", (*it).second);
		rt.add("q1");
		rt.add((*it).second);
		T.push_back(std::make_pair(lt,rt));
	}
	for(std::set<std::string>::iterator it = terminals.begin(); it != terminals.end(); ++it){
		lt = Triple<std::string>("q1", (*it), (*it));
		//rt = Tuple<std::string>("q1", "e");
		rt.add("q1");
		rt.add("e");
		T.push_back(std::make_pair(lt,rt));
	}
	pda.setT(T);


	_verbose->comment("PDA is created");
	_verbose->closeCurrentFunction();

	ENSURE(pda.isPDA(), "the PDA that is created is not a PDA");

	return pda;



}

/**
 * @brief validation for postcondition
 * @return true if it is properly initialized, if not than false
 */
bool CONV::properlyInitialized(){
	return (this == InitCheck);
}

/**
 * @brief  a function to create the productions for a given transition
 * @param currentState the transition were we now are.
 * @param nextState the next state by the trantition.
 * @param states all the states of the pda.
 * @param topStack the top of the stack of the pda.
 * @param newStack the new stack of the pda
 * @param the input for this transition.
 * @return the different productions for this transition
 */
std::set<std::pair<std::string, std::string> > CONV::createProductions(std::string currentState, std::string nextState, std::set<std::string> states, std::string topStack, std::string newStack, std::string input){
	_verbose->addFunction("CONV::createProductions(std::string currentState, std::string nextState, std::set<std::string> states, std::string topStack, std::string newStack, std::string input)");
	//pre-initialized data
	std::vector<std::string> combination;
	std::string s1;
	std::string s2;
	std::string s = currentState;
	std::string t =  nextState;
	std::stringstream leftOperant;
	std::stringstream rightOperant;
	std::set<std::pair<std::string, std::string> > transitions;
	std::vector<std::vector<std::string> > combinations = this->combinations(states);

	//if the newstack size is bigger than 1 we haven production with three variable
	if(newStack.size() > 1){
		std::stringstream ss;
		ss << newStack.at(0);
		std::string Y1 = ss.str();
		ss.str(std::string());
		ss << newStack.at(1);
		std::string Y2 = ss.str();
		std::vector<std::vector<std::string> > combinations = this->combinations(states);
		//make productions in the form [s Y sk]-> a[t Y1 s1][s1 Y2 s2] where s1 and s2 can be any combinations of the states
		for(unsigned int i = 0; i < combinations.size(); ++i){
			combination = combinations.at(i);
			s1 = combination.at(0);
			s2 = combination.at(1);
			leftOperant<<createOperant(s,topStack,s2);
			rightOperant<<input<<createOperant(t,Y1,s1)<<createOperant(s1,Y2,s2);
			transitions.insert(std::make_pair(leftOperant.str(), rightOperant.str()));
			leftOperant.str(std::string());
			rightOperant.str(std::string());
		}
	}
	//if the newStack is epsilon than we have a production with only the input as result
	else if(newStack == "e"){
		leftOperant<<createOperant(s,topStack,nextState);
		rightOperant<<input;
		transitions.insert(std::make_pair(leftOperant.str(), rightOperant.str()));
		leftOperant.str(std::string());
		rightOperant.str(std::string());
	}
	//else we have a production with only two variable
	else{
		for(std::set<std::string>::iterator it = states.begin(); it != states.end(); ++it){
			leftOperant<<createOperant(s,topStack,(*it));
			rightOperant<<input<<createOperant(nextState,topStack,(*it));
			transitions.insert(std::make_pair(leftOperant.str(), rightOperant.str()));
			leftOperant.str(std::string());
			rightOperant.str(std::string());
		}
	}
	_verbose->closeCurrentFunction();
	return transitions;
}

/**
	 *  @brief a function that creates the startProduction
	 *  @param states all the states of the pda.
	 *  @param startState the startState of the pda.
	 *  @param StartStackSymbol he start stack symbol of the pda.
	 *	@return the production for the transition.
	 */
std::set<std::pair<std::string, std::string> > CONV::createStartProduction(std::set<std::string> states, std::string startState, std::string startStackSymbol){
	_verbose->addFunction("CONV::createStartProduction(std::set<std::string> states, std::string startState, std::string startStackSymbol)");

	std::set<std::pair<std::string, std::string> > transitions;
	std::stringstream leftOperant;
	std::stringstream rightOperant;
	leftOperant << "S";
	for(std::set<std::string>::iterator it = states.begin(); it != states.end(); ++it){
		rightOperant<<createOperant(startState, startStackSymbol, (*it));
	}
	transitions.insert(std::make_pair(leftOperant.str(), rightOperant.str()));
	leftOperant.str(std::string());
	rightOperant.str(std::string());
	_verbose->closeCurrentFunction();
	return transitions;
}




/**
 * @brief a function that sets the operant in the right format
 * @param firstState the first element in the operant
 * @param stackSymbol the second element in the operant
 * @param secondState the thirth symbol in the operant
 * @return the right formated operant
 */
std::string CONV::createOperant(std::string firstState, std::string stackSymbol, std::string secondState){
	_verbose->addFunction("CONV::createOperant(std::string firstState, std::string stackSymbol, std::string secondState)");
	std::stringstream operant;

	operant<<"["<<firstState<<stackSymbol<<secondState<<"]";

	_verbose->closeCurrentFunction();
	return operant.str();
}



/**
 * @brief a function that creates al the combinations of the states
 * @param states al the states of this pda.
 * @return al the combinations.
 *
 */
std::vector<std::vector<std::string> > CONV::combinations(std::set<std::string> input){
	_verbose->addFunction("CONV::combinations(std::set<std::string> input)");
	std::vector<std::vector<std::string> > combinations;
	std::vector<std::string> combination;

	for(std::set<std::string>::iterator it = input.begin(); it != input.end(); ++it){
		for(std::set<std::string>::iterator _it = input.begin(); _it != input.end(); ++_it){
			combination.push_back((*it));
			combination.push_back((*_it));
			combinations.push_back(combination);
			combination.clear();
		}
	}
	_verbose->closeCurrentFunction();
	return combinations;



}

} /* namespace mnb */
