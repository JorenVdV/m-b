/**
 * @file CONV.h
 * @class mnb::CONV
 * @brief conversion class for PDA->CFG and CFG->PDA
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Nov 15, 2013
 */

#pragma once
#ifndef CONV_H_
#define CONV_H_

#include "../PDA/PDA.h"
#include "../CFG/CFG.h"
#include "../Verbose/Verbose.h"
#include "../TupleTriple/Triple.h"
#include "../TupleTriple/Tuple.h"
#include <string>
#include <set>
#include <sstream>
#include <iostream>
#include <algorithm>

namespace mnb {

class CONV {
public:

	CONV(bool verb = false);


	virtual ~CONV();


	PDA CFGtoPDA(CFG& cfg);


	CFG PDAtoCFG(PDA& pda);

	bool properlyInitialized();


private:


	std::set<std::pair<std::string, std::string> > createProductions(std::string currentState, std::string nextState, std::set<std::string> states, std::string topStack, std::string newStack, std::string input);


	std::set<std::pair<std::string, std::string> > createStartProduction(std::set<std::string> states, std::string startState, std::string startStackSymbol);

	std::string createOperant(std::string firstState, std::string stackSymbol, std::string secondState);


	std::vector<std::vector<std::string> > combinations(std::set<std::string> states);


	Verbose* _verbose;
	CONV* InitCheck;
};

} /* namespace mnb */
#endif /* CONV_H_ */
