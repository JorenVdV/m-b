/**
 * @file PDA.cpp
 * @class mnb::PDA
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Nov 15, 2013
 */

#include "PDA.h"

namespace mnb {

/**
 * @brief constructs a PDA from a given XML-file adds a verbose mode to the functions
 * @param string fileName, path and name of XML-file to parse
 * @param bool verbose, gives additional output for debugging, standard set to false.
 * @post object must be properly initialized
 * @see properlyInitialized()
 */
PDA::PDA(std::string fileName, bool verb) : _verbose(_verbose->instance(verb)), InitCheck(this), _maxStackSize(0), _graph(new Node()){
	this->parse(fileName);
	ENSURE(this->properlyInitialized(), "the pda is not properlyInitialized");
}

/**
 * @brief a default constructor
 * @post object must be properly initialized
 * @see properlyInitialized()
 */
PDA::PDA() : _verbose(_verbose->instance(false)), InitCheck(this), _maxStackSize(0), _graph(new Node()){
	ENSURE(this->properlyInitialized(), "the converter is not properlyInitialized");
}


/**
 * @brief standard destructor
 */
PDA::~PDA(){

}

/**
 * @brief returns whether or not given automat is a PDA
 * @return bool true if it is PDA, false if not
 */
bool PDA::isPDA(){
	_verbose->addFunction("PDA::isPDA()");
	REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
	REQUIRE(this->notEmpty(), "there is a variable that is emtpy");

	//check if q0 is in Q
	_verbose->comment("[0/4] check if q0 is in Q");
	if(_Q.find(_q0) == _Q.end()){
		_verbose->comment("q0 is not in Q, thus this is not a valid PDA");
		_verbose->closeCurrentFunction();
		return false;
	}

	//check if z0 is in S
	_verbose->comment("[1/4] check if z0 is in S");
	if(_S.find(_Z0) == _S.end()){
		_verbose->comment("Z0 is not in S, thus this is not a valid PDA");
		_verbose->closeCurrentFunction();
		return false;
	}

	//check if F is in Q
	_verbose->comment("[2/4] check if F is in Q");
	for(std::set<std::string>::iterator it = _F.begin(); it != _F.end(); ++it ){
		if(_Q.find(*it) == _Q.end()){
			_verbose->comment("F is not in Q, thus this is not a valid PDA");
			_verbose->closeCurrentFunction();
			return false;
		}
	}

	//pre-initialized data
	Triple<std::string> lt;
	Tuple<std::string> rt;
	std::set<std::string> S = _S;
	S.insert("e");
	std::set<std::string> E = _E;
	E.insert("e");
	std::stringstream ss;
	int numOfChar = 0;

	//check if T is in Q and S
	_verbose->comment("[3/4] check if T is in Q and S");
	for(unsigned int i = 0; i < _T.size(); ++i){
		lt = _T.at(i).first;
		rt = _T.at(i).second;
		//check is ltQ and rtQ is in Q
		if(_Q.find(lt.first()) == _Q.end() || _Q.find(rt.first()) == _Q.end()){
			_verbose->comment("T is not in Q, thus this is not a valid PDA 1");
			_verbose->closeCurrentFunction();
			return false;
		}
		//check if ltI is in E
		if(E.find(lt.second()) == E.end()){
			_verbose->comment("T is not in E, thus this is not a valid PDA 2");
			_verbose->closeCurrentFunction();
			return false;
		}
		//check is ltS is in S
		if(S.find(lt.thirth()) == S.end()){
			_verbose->comment("T is not in S, thus this is not a valid PDA 3");
			_verbose->closeCurrentFunction();
			return false;
		}
		//check if rtS is in S
		for(std::set<std::string>::iterator t_it = S.begin(); t_it != S.end(); ++t_it){
			for(unsigned int i = 0 ;i < rt.second().size() ;i++)
			{
				ss << rt.second().at(i);
				if(ss.str() == (*t_it))
				{
					numOfChar++;
				}
				ss.str(std::string());
			}
		}
		if(numOfChar != rt.second().size()){
			_verbose->comment("T is not in S, thus this is not a valid PDA 4");
			_verbose->closeCurrentFunction();
			return false;
		}
		numOfChar = 0;
	}
	_verbose->comment("[4/4] this is a valid PDA");
	_verbose->closeCurrentFunction();
	return true;
}

/**
 * @brief the PDA will be written to screen
 * @pre object must be properly initialized
 * @pre the variables may not be empty
 * @throw this is not a valid pda
 * @see properlyInitialized()
 * @see notEmpty()
 * @see isPDA()
 */
void PDA::print(){
	try{
		REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
		REQUIRE(this->notEmpty(), "there is a variable that is emtpy");
		if(!this->isPDA()) throw "this is not a valid pda";

		std::cout<<"====================PDA============================="<<std::endl;
		std::cout<<"states = {";
		std::set<std::string>::iterator itQ = _Q.begin();
		std::cout<<*itQ++;
		for(;itQ != _Q.end(); itQ++)
			std::cout<<", "<<*itQ;
		std::cout<<"}"<<std::endl;

		std::cout<<"input alphabet = {";
		std::set<std::string>::iterator itE = _E.begin();
		std::cout<<*itE++;
		for(;itE != _E.end(); itE++)
			std::cout<<", "<<*itE;
		std::cout<<"}"<<std::endl;

		std::cout<<"stack alphabet = {";
		std::set<std::string>::iterator itS = _S.begin();
		std::cout<<*itS++;
		for(;itS != _S.end(); itS++)
			std::cout<<", "<<*itS;
		std::cout<<"}"<<std::endl;

		std::cout<<"start state = "<<_q0<<std::endl;

		std::cout<<"start stack symbol = "<<_Z0<<std::endl;

		std::cout<<"accepting states = {";
		std::set<std::string>::iterator itF = _F.begin();
		std::cout<<*itF++;
		for(;itF != _F.end(); itF++)
			std::cout<<", "<<*itF;
		std::cout<<"}"<<std::endl;

		std::cout<<"transitions = {"<<std::endl;
		for(auto it = _T.begin(); it != _T.end(); ++it ){
			Tuple<std::string> lP = it->second;
			Triple<std::string> rP = it->first;
			std::cout<<"               "<<"("<<rP.first()<<" "<<rP.second()<<" "<<rP.thirth()<<") -> ("<<lP.first()<<" "<<lP.second()<<")"<<std::endl;
		}
		std::cout<<"               }"<<std::endl;


		std::cout<<"===================================================="<<std::endl;

	}catch(const char* err){
		_verbose->error(err);
		exit (EXIT_FAILURE);
	}
}

/**
 * @brief the PDA will be written to a DOT-File.
 * @pre object must be properly initialized
 * @pre the variables may not be empty
 * @throw this is not a valid pda
 * @see properlyInitialized()
 * @see notEmpty()
 * @see isPDA()
 */
void PDA::printToDOT(){
	try{
		REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
		REQUIRE(this->notEmpty(), "there is a variable that is emtpy");
		if(!this->isPDA()) throw "this is not a valid pda";

		_verbose->addFunction("PDA::printToDOT()");

		//create a file in the bin map, named "PDA.dot"
		std::ofstream myfile ("PDA.dot");

		//Check if file is open
		if(myfile.is_open()){
			_verbose->comment("PDA.dot is open");
			//pre initialized data
			std::map<std::string, std::string>  transition;
			std::map<std::string, std::string>::iterator f_it;
			std::stringstream  transitionData;
			std::string value;
			std::string label;
			Triple<std::string> lt;
			Tuple<std::string> rt;
			_verbose->comment("default values are inserted in the file");

			//default values of our files.
			myfile<<"digraph PDA {\n";
			myfile<<"rankdir=LR;\n";
			myfile<<"ranksep=0.5;\n";
			myfile<<"node [ style = invis, shape = rect ]; new;\n";
			_verbose->comment("shapes are set in the file");
			//setting the shape of the accpting states to double circles
			for(std::set<std::string>::iterator it = _F.begin(); it != _F.end(); ++it ){
				myfile<<"node [style = rounded, shape = doublecircle, color=black, fontcolor=black]; ";
				myfile<<*it<<"; \n";
			}

			//set the shape of the rest to a single circle.
			myfile<<"node [ style = rounded, shape = circle, color=black, fontcolor=black ];\n";
			_verbose->comment("the start state is set");
			//set a single arrow to the start state
			myfile<<"new -> "<<_q0<<"[ label = \"start\"];\n";
			_verbose->comment("labels are set");
			//loop through the transitions and check wich transition start from the same state and goes to the same state
			//these labels should be appended to one another.
			for(unsigned int i = 0 ; i < _T.size(); ++i){
				lt = _T.at(i).first;
				rt = _T.at(i).second;

				//empty the pre initialized data
				transitionData.str(std::string());
				value = "";

				//make a unique identifier
				value.append(lt.first()).append(rt.first());

				//check if this identifier doesn't exist in the set, if it doesn't we need to create a new entry
				//otherwise we need to append the labels with a new line token.
				if(transition.count(value) == 0){

					//create the transition with a label
					transitionData <<lt.first()<<" -> "<<rt.first();
					transitionData << "[ label = \""<<lt.second()<<";"<<lt.thirth()<<"/"<<rt.second();

					//insert the new data in the set
					transition.insert(std::make_pair(value, transitionData.str()));
				}
				else{
					//find the iterator to the right value
					f_it = transition.find(value);

					//create the new label
					transitionData << "\\n"<<lt.second()<<";"<<lt.thirth()<<"/"<<rt.second();

					//append the new label to the old label
					f_it->second.append(transitionData.str());

				}
			}

			//iterate over the transitions and close the labels and add a new line token
			for(std::map<std::string, std::string>::iterator it = transition.begin(); it != transition.end(); ++it ){
				myfile<<it->second<<"\"]\n";
			}
			//add the final enter and the file is done.
			myfile<<"}\n";
		}
		else{
			throw "Unable to open the file";

		}
		_verbose->comment("PDA.dot is finished and closed");
		//close the file suchthat the data is freed and the file accessible.
		myfile.close();
		//close the verbose in the current function.
		_verbose->closeCurrentFunction();
	}catch(const char* err){
		_verbose->error(err);
		exit (EXIT_FAILURE);
	}

}

/**
 * @brief the PDA will be written to a XML-file.
 * @param string fileName the name of the file
 * @param string path the path where the file should be written, standard set to standard location.
 * @pre object must be properly initialized
 * @pre the variables may not be empty
 * @throw this is not a valid pda
 * @see properlyInitialized()
 * @see isPDA()
 *
 */
void PDA::printToXML(std::string fileName, std::string path){
	_verbose->addFunction("CFG::printToXML(std::string fileName, std::string path)");
	REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
	REQUIRE(this->notEmpty(), "there is a variable that is emtpy");
	if(!this->isPDA()) throw "this is not a valid pda";

	//create new XML doc
	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();

	//creating xml version and encoding
	_verbose->comment("[0/8]creating xml version and encoding");
	tinyxml2::XMLNode* rootNode = doc->InsertFirstChild( doc->NewDeclaration());

	//insert <PDA> element
	_verbose->comment("[1/8] PDA-root node created");
	tinyxml2::XMLNode* pda = doc->InsertAfterChild(rootNode,doc->NewElement( "PDA" )) ;

	//adding states
	_verbose->comment("[2/8] adding states");
	tinyxml2::XMLNode* states = pda->InsertFirstChild(doc->NewElement( "States" )) ;
	for(std::set<std::string>::iterator it = _Q.begin(); it != _Q.end(); ++it){
		tinyxml2::XMLNode* q = states->InsertEndChild(doc->NewElement( "Q" ));
		q->InsertFirstChild( doc->NewText( (*it).c_str()));
	}

	//adding start state
	_verbose->comment("[3/8] adding start state");
	tinyxml2::XMLNode* startState = pda->InsertEndChild(doc->NewElement( "StartState" ) );
	startState->InsertFirstChild(doc->NewText(_q0.c_str()));

	//adding accepting states
	_verbose->comment("[4/8] adding accepting states");
	tinyxml2::XMLNode* acceptingStates = pda->InsertEndChild(doc->NewElement( "AcceptingStates" ) );
	for(std::set<std::string>::iterator it = _F.begin(); it != _F.end(); ++it){
		tinyxml2::XMLNode* f = acceptingStates->InsertEndChild(doc->NewElement( "F" ));
		f->InsertFirstChild( doc->NewText( (*it).c_str()));
	}

	//adding input alphabet
	_verbose->comment("[5/8] adding input alphabet");
	tinyxml2::XMLNode* inputAlphabet = pda->InsertEndChild(doc->NewElement( "InputAlphabet" ) );
	for(std::set<std::string>::iterator it = _E.begin(); it != _E.end(); ++it){
		tinyxml2::XMLNode* e = inputAlphabet->InsertEndChild(doc->NewElement( "E" ));
		e->InsertFirstChild( doc->NewText( (*it).c_str()));
	}

	//adding stack alphabet;
	_verbose->comment("[6/8] adding stack alphabet");
	tinyxml2::XMLNode* stackAlphabet = pda->InsertEndChild(doc->NewElement( "StackAlphabet" ) );
	for(std::set<std::string>::iterator it = _S.begin(); it != _S.end(); ++it){
		tinyxml2::XMLNode* e = stackAlphabet->InsertEndChild(doc->NewElement( "S" ));
		e->InsertFirstChild( doc->NewText( (*it).c_str()));
	}

	//adding start stack symbol
	_verbose->comment("[7/8] adding start stack symbol");
	tinyxml2::XMLNode* startStackSymbol = pda->InsertEndChild(doc->NewElement( "StartStackSymbol" ) );
	startStackSymbol->InsertFirstChild(doc->NewText(_Z0.c_str()));

	//pre-initialized data
	Triple<std::string> lt;
	Tuple<std::string> rt;
	//adding transition
	_verbose->comment("[8/8] adding transitions");
	tinyxml2::XMLNode* transitions = pda->InsertEndChild(doc->NewElement( "Transitions" ) );
	for(unsigned int i = 0 ; i < _T.size(); ++i){
		tinyxml2::XMLNode* t = transitions->InsertEndChild(doc->NewElement( "T" ));
		lt = _T.at(i).first;
		rt = _T.at(i).second;
		tinyxml2::XMLNode* lT = t->InsertEndChild(doc->NewElement( "lT" ));
		lT->InsertEndChild(doc->NewElement("ltQ"))->InsertFirstChild(doc->NewText(lt.first().c_str()));
		lT->InsertEndChild(doc->NewElement("ltI"))->InsertFirstChild(doc->NewText(lt.second().c_str()));
		lT->InsertEndChild(doc->NewElement("ltS"))->InsertFirstChild(doc->NewText(lt.thirth().c_str()));;
		tinyxml2::XMLNode* rT = t->InsertEndChild(doc->NewElement( "rT" ));
		rT->InsertEndChild(doc->NewElement("rtQ"))->InsertFirstChild(doc->NewText(rt.first().c_str()));
		rT->InsertEndChild(doc->NewElement("rtS"))->InsertFirstChild(doc->NewText(rt.second().c_str()));
	}
	//save the doc
	if(path.empty())
		doc->SaveFile(fileName.append(".xml").c_str());
	else{
		doc->SaveFile(path.append("/").append(fileName).append(".xml").c_str());
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief function that simulates the PDA, it checks if the pda accepts the string, and make file with a'l the ID's
 * @param string a for input.
 * @return true if accept string, false if don't
 * @pre object must be properly initialized
 * @pre the variables may not be empty
 * @throw this is not a valid pda
 * @see properlyInitialized()
 * @see notEmpty()
 * @see isPDA()
 */
bool PDA::simulate(std::string a){
	try{
		REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
		REQUIRE(this->notEmpty(), "there is a variable that is emtpy");
		if(!this->isPDA()) throw "this is not a valid pda";
		//bool so that we no if the input is accepted
		bool accepted = false;
		_maxStackSize = a.size();
		_verbose->addFunction("PDA::simulate(std::string a)");
		//pre-initialized data
		std::string check;
		std::stringstream s;
		_verbose->comment("[0/4] create root");
		//creating the root
		PDASimulation root;
		root.stack.push(_Z0);
		root.currentState = _q0 ;
		std::set<std::string> E = _E;
		E.insert("e");
		for (std::string::reverse_iterator rit=a.rbegin(); rit!=a.rend(); ++rit){
			s<< (*rit);
			if(E.find(s.str()) == E.end()){
				throw "no valid input";
			}
			root.input.push(s.str());
			s.str(std::string());
		}
		//pre-initialized data
		std::pair<PDASimulation, Node*>  n;
		std::stack<std::pair<PDASimulation, Node*> > stack;
		std::vector<PDASimulation> childeren;
		_verbose->comment("[1/4] create data structure");
		//create the data structure
		_graph = new Node();
		PDASimulation c;
		PDASimulation final;
		stack.push(std::make_pair(root, _graph));
		_graph->setState(root);
		std::stringstream ss;
		ss<<"root: "<< root.toString()<<"\n";
		_verbose->comment("[2/4] simulate");;
		//simulate the PDA
		while(!stack.empty()){
			n = stack.top();
			stack.pop();
			//fid the childeren of every parent
			childeren = findChilderenOf(n.first);
			s<<"parent :"<< n.first.toString()<<"\n";
			//set the childeren to the parrent
			for(unsigned int i = 0; i < childeren.size(); ++i){
				c = childeren.at(i);
				Node* node = new Node();
				node->setState(c);
				n.second->addchild(node);
				//push the new children to the stack because these are maybe also parents
				stack.push(std::make_pair(c, node));
				ss<<"child :"<<c.toString()<<"\n";
			}
			//if the node has no childeren than we can go anny futher and we need to check if wit is accepted or if we are stuck
			if(childeren.empty()){
				final = n.first;
				//if the state is in the set of endstates and the top stack symbol is the start symbol and the input is e than we accept
				//else we have a deadend
				if((_F.find(final.currentState) != _F.end()) && (final.stack.top() == _Z0) && (final.input.top() == "e") ){
					ss<<"Accepted\n"<<"\n";
					ss<<"root: "<< root.toString()<<"\n";
					accepted = true;
				}
				else{
					ss<<"Dead end\n"<<"\n";
					ss<<"root: "<< root.toString()<<"\n";
				}
			}
		}
		//create a file in the bin map, named "TransitionTable.txt"
		std::ofstream myfile ("ID.txt");
		//Check if file is open
		_verbose->comment("[3/4] printing the table to a text file (ID.txt)");
		if(myfile.is_open()){
			myfile<<ss.rdbuf();
			myfile.close();
		}
		_verbose->comment("[4/4] check is the input is accepted");
		//set extra indentation
		(*_verbose)++;
		if(accepted){
			std::cout<<"The input "<<a<<" is ACCEPTED"<<std::endl;
			_verbose->closeCurrentFunction();
			return true;
		}
		else{
			std::cout<<"The input "<<a<<" is NOTACCEPTED"<<std::endl;
			_verbose->closeCurrentFunction();
			return false;
		}
	}
	catch(const char* err){
		_verbose->error(err);
		exit (EXIT_FAILURE);
	}


}

/**
 * @brief print the given transition functions for this PDA, in table form.
 * @pre object must be properly initialized
 * @pre the variables may not be empty
 * @throw this is not a valid pda
 * @see properlyInitialized()
 * @see notEmpty()
 * @see isPDA()
 */
void PDA::transitionToTable(){
	_verbose->addFunction("PDA::transitionToTable()");
	REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
	REQUIRE(this->notEmpty(), "there is a variable that is emtpy");
	if(!this->isPDA()) throw "this is not a valid pda";

	//pre-initialized data
	std::stringstream ss;
	std::set<std::string> E = _E;
	E.insert("e");
	Triple<std::string> lt;
	Tuple<std::string> rt;
	std::stringstream size;
	int columnSize = -1;
	int t_size;
	std::vector<std::pair< std::string, std::string> > order;
	std::stringstream t_ss;
	int t_columnSize;
	std::pair<Triple<std::string>, Tuple<std::string> > possibleTransition;
	bool found = false;
	_verbose->comment("Calulate the max columnwidth");
	//check what the max columnsize is, this columnsize should be used
	for(unsigned int j = 0; j < _T.size(); ++j){
		rt = _T.at(j).second;
		size << rt.first() <<","<<rt.second();
		t_size = size.str().size();
		if(t_size>columnSize)
			columnSize = t_size;
		size.str(std::string());

	}
	_verbose->comment("Start with the table");
	//start the table
	for(unsigned int i = 0; i < columnSize; ++i){
		ss<<" ";
	}
	ss<<"|";
	_verbose->comment("[0/4] create the headers");
	//add all the combinations of input symbols and top of stack symbols to the top row/header
	for(std::set<std::string>::iterator it = E.begin(); it != E.end(); ++it){
		for(std::set<std::string>::iterator s_it = _S.begin(); s_it != _S.end(); ++s_it){
			//use a temp stringstream so that we know the size of this column
			t_ss<<" "<<(*it)<<","<<(*s_it);
			//check what the size is of the current column
			t_columnSize = t_ss.str().size();
			//add the pairs of the different input symbols and the top of stack symbols
			//so that we can use it later
			order.push_back(std::make_pair((*it), (*s_it)));
			//add additional spaces to the column so that the size is everywhere the same
			for(unsigned int i = t_columnSize; i < columnSize; ++i){
				t_ss<<" ";
			}
			//push the temp string stream in the real string stream
			ss<<t_ss.str();
			ss<<"|";
			//clear temp stringstream
			t_ss.str(std::string());
		}
	}
	//what is the with of the total table
	int width = ss.str().size();
	ss<<"\n";
	_verbose->comment("[1/4] create the lines");
	//add the line for the new row
	for(unsigned int i = 0; i < width; ++i){
		if(ss.str().at(i) == '|')
			ss<<"+";
		else
			ss<<"-";
	}
	//go to the new row
	ss<<"\n";
	//start with adding useful data
	_verbose->comment("[2/4] adding data to the table");
	for(std::set<std::string>::iterator it = _Q.begin(); it != _Q.end(); ++it){
		//set the states in the first column
		//if the state is the start symbol than it should start with a arrow
		if((*it) == _q0.c_str()){
			t_ss<<"->"<<(*it);
		}
		//if the state is an accepting state than it start with a star
		else if(_F.find((*it)) != _F.end()){
			t_ss<<" *"<<(*it);
		}
		//els the state should not start with anything.
		else{
			t_ss<<"  "<<(*it);
		}
		//the size of the current column
		t_columnSize = t_ss.str().size();
		//add additional spaces to the column so that the size is everywhere the same
		for(unsigned int i = t_columnSize; i < columnSize; ++i){
			t_ss<<" ";
		}
		ss<<t_ss.str();
		t_ss.str(std::string());
		//loop over the different combinations of input symbols and top of stack symbols
		for(unsigned int i = 0; i < order.size(); ++i){
			//loop throught the transitions and check witch of the transions we need to use
			//if we found hem than we let it sow with the boolean found
			for(unsigned int j = 0; j < _T.size() ; ++j){
				lt = _T.at(j).first;
				rt = _T.at(j).second;
				if((lt.first() == (*it)) && (lt.second() == order.at(i).first) && (lt.thirth() == order.at(i).second)){
					possibleTransition = std::make_pair(lt, rt);
					found = true;
				}
			}
			rt = possibleTransition.second;
			ss<<"|";
			//if we found the correct transition we need to put it in the column
			//otherwise we need to leave the column blank
			//we use a temp stringstream so that we can check the size of the current column
			if(found)
				t_ss<<rt.first()<<","<<rt.second();
			else
				t_ss<<"";
			//check what the size is of the current column
			t_columnSize = t_ss.str().size();
			//add additional spaces to the column so that the size is everywhere the same
			for(unsigned int k = t_columnSize; k < columnSize; ++k){
				t_ss<<" ";
			}
			//put the temporary stringstream in the real one
			ss<<t_ss.str();
			//clear the temporary stringstream
			t_ss.str(std::string());
			//set the bool back to false
			found = false;
		}
		//close the row
		ss<<"|";
		//and we go to the new row
		ss<<"\n";
		//add the line for the new row
		if(it != --_Q.end()){
			for(unsigned int i = 0; i < width; ++i){
				if(ss.str().at(i) == '|')
					ss<<"+";
				else
					ss<<"-";
			}
			ss<<"\n";
		}
	}
	_verbose->comment("[3/4] printing the table to terminal:");
	std::cout<<ss.str()<<std::endl;
	//create a file in the bin map, named "TransitionTable.txt"
	std::ofstream myfile ("TransitionTable.txt");
	//Check if file is open
	_verbose->comment("[4/4] printing the table to a text file (TransitionTable.txt)");
	if(myfile.is_open()){
		myfile<<ss.rdbuf();
		myfile.close();
	}
	else{
		throw "Unable to open the file";
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief validation for postcondition
 * @return true if it is properly initialized, if not than false
 */
bool PDA::properlyInitialized(){
	return (this == InitCheck);
}

/**
 * @brief get the alphabet of the PDA
 * @return the set of the elements of the alphabet
 */
const std::set<std::string>& PDA::getE() const {
	return _E;
}

/**
 * @brief set the alphabet of the PDA
 * @param set<string> e the set of the elements of the alphabet
 */
void PDA::setE(const std::set<std::string>& e) {
	_E = e;
}

/**
 * @brief gets all the accepting states of the PDA
 * @return all the accepting states of the PDA
 */
const std::set<std::string>& PDA::getF() const {
	return _F;
}

/**
 * @brief sets all the accepting states of the PDA
 * @param set<string> f all the accepting states of the PDA
 */
void PDA::setF(const std::set<std::string>& f) {
	_F = f;
}

/**
 * @brief gets all the states of the PDA
 * @return all the states of the PDA
 */
const std::set<std::string>& PDA::getQ() const {
	return _Q;
}

/**
 * @brief sets all the states of the PDA
 * @param set<string> q all the states of the PDA
 */
void PDA::setQ(const std::set<std::string>& q) {
	_Q = q;
}

/**
 * @brief gets the start state of the PDA
 * @return the start state
 */
const std::string& PDA::getQ0() const {
	return _q0;
}

/**
 * @brief sets the start state of the PDA
 * @param string q0 the start state
 */
void PDA::setQ0(const std::string& q0) {
	_q0 = q0;
}

/**
 * @brief gets the stack alphabet
 * @return the set of the elements of the alphabet
 */
const std::set<std::string>& PDA::getS() const {
	return _S;
}

/**
 * @brief sets the stack alphabet
 * @param set<string> s the set of the elements of the alphabet
 */
void PDA::setS(const std::set<std::string>& s) {
	_S = s;
}

/**
 * @brief gets all the transition of the PDA
 * @return all the trantitions of the PDA
 */
const std::vector<std::pair<Triple<std::string>, Tuple<std::string> > >& PDA::getT() const {
	return _T;
}

/**
 * @brief sets all the trantitions of the PDA
 * @param vector<pair<Triple<string>, Tuple<string>>> t a vector of all the transitions
 */
void PDA::setT(
		const std::vector<std::pair<Triple<std::string>, Tuple<std::string> > >& t) {
	_T = t;
}

/**
 * @brief gets the start stack symbol of the PDA
 * @return the start stack symbol
 */
const std::string& PDA::getZ0() const {
	return _Z0;
}

/**
 * @brief sets the start stack symbol of the PDA
 * @param string z0 the start stack symbol
 */
void PDA::setZ0(const std::string& z0) {
	_Z0 = z0;
}

/**
 * @brief parse the xml so that is become a PDA
 * @pre object must be properly initialized
 * @pre file must exists.
 * @see properlyInitialized()
 * @pre the file is valid xml file and a valid path
 * @post nothing may be empty empty
 * @see properlyInitialized()
 */
void PDA::parse(std::string& fileName){
	_verbose->addFunction("PDA::parse(std::string& fileName)");
	REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
	try{
		tinyxml2::XMLDocument doc;
		//check if file exists
		REQUIRE(doc.fileExists(fileName), "file does not exists");
		//load xml file
		doc.LoadFile(fileName.c_str());

		//temp data for correct verbose output
		std::string elementName;
		std::string message;

		//search the root node
		tinyxml2::XMLNode *rootNode = doc.FirstChild();
		if(!rootNode) throw "There was no root node found.";
		elementName = std::string(rootNode->Value());
		message = "Found the root node: <" + elementName + ">";
		_verbose->comment(message);

		//search the sibling after the root
		tinyxml2::XMLNode* siblingNode = rootNode ->NextSibling();
		if(!siblingNode) throw "there is no siblingNode for the rootNode";
		elementName = std::string(siblingNode->Value());
		message = "Found the sibling node: <" + elementName + ">";
		_verbose->comment(message);

		//get the first child of the PDA element
		tinyxml2::XMLNode* childNode = siblingNode ->FirstChild();
		if(!childNode) throw "there is no childNode for <PDA>";

		//loop over all the childeren of the pda element, as long as there are siblings
		while(childNode){
			elementName = std::string(childNode->Value());
			message = "found element " + elementName;
			_verbose->comment(message);
			//check if child of the pda element has children of his own
			//if yes than we need to add multiple elements else we need to add just one
			if(childNode->FirstChild()->FirstChild() == NULL){
				readElement(childNode);
			}
			else{
				readElements(childNode);
			}
			//go to the next child of the pda element
			childNode = childNode->NextSibling();
		}
		ENSURE(this->notEmpty(), "there is still a variable that is empty");
		//check if the file describe a valid pushdown automata
		if(!this->isPDA()) throw "the input file doens't describe a valid PDA";
	}catch(const char* err){
		_verbose->error(err);;
		exit (EXIT_FAILURE);

	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to read a element with mulitiply childeren.
 * @param tinyxml2::XMLNode* root the root of the current location in the xml.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @throw "Encountered a unrecognizable element";
 * @see properlyInitialized()
 *
 */
void PDA::readElements(tinyxml2::XMLNode* root){
	_verbose->addFunction("PDA::readElements(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "elements could not be parsed, not properly initialized");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";
	//get the name of the current element
	std::string elementName = root->Value();
	//pre-initialized data so that it does not need to be initialized in the while loop
	tinyxml2::XMLNode* childNode = root->FirstChild();
	tinyxml2::XMLElement *childElmtl;
	std::string data;
	std::string message;
	//loop over the child elements of the current element, as long as there are siblings
	while(childNode){
		//if it is the transitions value then we need to call a extra function, otherwise we can handle it here
		if(elementName != "Transitions"){
			childElmtl = childNode->ToElement();
			//the data of this element
			data = std::string(childElmtl->GetText());
			//add the the data to the object variable
			if(elementName == "States"){
				message = "Add " + data + " to States";
				_verbose->comment(message);
				_Q.insert(data);
			}
			else if(elementName == "AcceptingStates"){
				message = "Add " + data + " to AcceptingStates";
				_verbose->comment(message);
				_F.insert(data);
			}
			else if(elementName == "InputAlphabet"){
				message = "Add " + data + " to InputAlphabet";
				_verbose->comment(message);
				_E.insert(data);
			}
			else if(elementName == "StackAlphabet"){
				message = "Add " + data + " to StackAlphabet";
				_verbose->comment(message);
				_S.insert(data);
			}
			else{
				throw "Encountered a unrecognizable element";
			}
		}
		else{
			readTransitie(childNode);
		}
		//get the next sibling of the current element
		childNode = childNode->NextSibling();
	}
	_verbose->closeCurrentFunction();

}
/**
 * @brief a function to read a element with just one child.
 * @param tinyxml2::XMLNode* root the root of the current location in the xml.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @throw "Encountered a unrecognizable element";
 * @see properlyInitialized()
 *
 */
void PDA::readElement(tinyxml2::XMLNode* root){
	_verbose->addFunction("PDA::readElement(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "element could not be parsed, not properly initialized");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";
	//get the name of the current element
	std::string elementName = root->Value();
	//get the first child of the current element
	tinyxml2::XMLNode* childNode = root->FirstChild();
	//temp data for correct verbose output
	std::string data;
	std::string message;
	//the data of this element
	data = std::string(childNode->Value());
	//add the the data to the object variable
	if(elementName == "StartState"){
		message = "Add " + data + " to q0";
		_verbose->comment(message);
		_q0 = data;
	}
	else if(elementName == "StartStackSymbol"){
		message = "Add " + data + " to Z0";
		_verbose->comment(message);
		_Z0 = data;
	}
	else{
		throw "Encountered a unrecognizable element";
	}
	_verbose->closeCurrentFunction();



}

/**
 * @brief a function to read the transitie elements
 * @param tinyxml2::XMLNode* root the root of the current location in the xml.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @see properlyInitialized()
 *
 */
void PDA::readTransitie(tinyxml2::XMLNode* root){
	_verbose->addFunction("PDA::readTransitie(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "transitie could not be parsed, not properly initialized");
	//check if the root node is null
	if(!root) throw "root pointer is null";
	//pre-initialized data
	tinyxml2::XMLNode* childNode = root->FirstChild();
	tinyxml2::XMLNode* childSiblNode = childNode->NextSibling();
	childNode = childNode->FirstChild();
	childSiblNode = childSiblNode->FirstChild();
	tinyxml2::XMLElement *childElmtl;
	tinyxml2::XMLElement *childSiblElmtl;
	std::string data;
	std::string message;
	Triple<std::string> triple;
	Tuple<std::string> tuple;
	//parse erything from lt and put it in the triple.
	while(childNode){
		childElmtl = childNode->ToElement();
		data = std::string(childElmtl->GetText());
		triple.add(data);
		childNode = childNode->NextSibling();
	}
	//parse everything from the rt and put it in the tuple
	while(childSiblNode){
		childSiblElmtl = childSiblNode->ToElement();
		data = std::string(childSiblElmtl->GetText());
		tuple.add(data);
		childSiblNode = childSiblNode ->NextSibling();
	}
	message = "Add " + triple.toString() + "-->" + tuple.toString() + " to Transition";
	_verbose->comment(message);
	//add the the data to the object variable
	_T.push_back(std::make_pair(triple, tuple));
	triple.clear();
	tuple.clear();
	_verbose->closeCurrentFunction();
}

/**
 * @brief helper function for class PDA
 * check all datamembers on emptyness
 * @return bool whether or not one of the members is empty
 * @pre object must be properly initialized
 * @see properlyInitialized()
 */
bool PDA::notEmpty(){
	//verbose begin function
	// pre condition
	REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");

	if(_Q.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("Q is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_E.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("E is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_S.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("S is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_T.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("T is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_q0.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("q0 is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_Z0.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("Z0 is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else if(_F.empty()){
		_verbose->addFunction("PDA::notEmpty()");
		_verbose->error("F is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	else {
		return true;
	}


}

/**
 * @brief helper function for class PDA
 * finds the childeren of a parent
 * @param PDASimulation parent the node for wich we need to find the childeren
 * @return std::vector<PDASimulation>  the childeren of the parent
 * @pre object must be properly initialized
 * @see properlyInitialized()
 */
std::vector<PDASimulation> PDA::findChilderenOf(PDASimulation parent){
	_verbose->addFunction("PDA::findChilderenOf(PDASimulation parent)");
	REQUIRE(this->properlyInitialized(), "PDA could not be parsed, not properly initialized");
	//pre-initialized data
	PDASimulation child;
	Triple<std::string> lt;
	Tuple<std::string> rt;
	std::stack<std::string> stack;
	std::vector<PDASimulation> childeren = std::vector<PDASimulation>();
	std::stack<std::string> input;
	std::string currentState;
	std::stringstream s;
	std::string newStack;
	//loop over all the possible transitions
	_verbose->comment("[0/1] collect all the transitions");
	_verbose->comment("[0/2] find childeren");
	for(unsigned int j = 0; j < _T.size(); ++j){
		lt = _T.at(j).first;
		rt = _T.at(j).second;
		//if the current state and the top of the stack en the top of the input are the same than we have a transition we need
		_verbose->comment("check if this is the right function");
		if((lt.first() == parent.currentState) &&( lt.second() == parent.input.top() || lt.second() == "e") && (lt.thirth() == parent.stack.top())){
			//the current state becomes the state of the result of the transition
			currentState = rt.first();
			//if stack result variable of the transition has a size of 2 than we need to push on the stack
			_verbose->comment("check if we need to pop the stack");
			if(rt.second() == "e"){
				stack = parent.stack;
				if(stack.size() == 1 ){
					stack.pop();
					stack.push(_Z0);
				}else if(stack.top() == _Z0){

				}else{
					stack.pop();
				}
			}
			else{
				newStack = rt.second();
				stack = parent.stack;
				stack.pop();
				for (std::string::reverse_iterator rit=newStack.rbegin(); rit!=newStack.rend(); ++rit){
					s<< (*rit);
					stack.push(s.str());
					s.str(std::string());
				}
			}
			//if the possible input is epsilon than we can go whit out notice to the next state, the input stays the same
			//else we need to pop the input, if the input becomes empty we add the epsilon symbol
			_verbose->comment("check if we have consumed input");
			if(lt.second() == "e"){
				input = parent.input;
			}
			else{
				input = parent.input;
				if(input.size() == 1){
					input.pop();
					input.push("e");
				}
				else if (input.top() == "e"){

				}else{
					input.pop();
				}
			}
			//the information of the state is set together
			child.stack = stack;
			child.input = input;
			child.currentState = currentState;
			//if the child is not equal to its parent we need to mark as its child
			_verbose->comment("check if child is not equal to parent");
			if(child != parent){
				_verbose->comment("child found");
				_verbose->comment("check if the stack size is not extreme great");
				if(child.stack.size() < _maxStackSize)
					childeren.push_back(child);
			}
		}
		else{
			_verbose->comment("not the right function");
		}

	}
	_verbose->closeCurrentFunction();
	return childeren;

}



} /* namespace mnb */


