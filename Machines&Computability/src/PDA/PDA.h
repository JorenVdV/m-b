/**
 * @file PDA.h
 * @class mnb::PDA
 * @brief PDA simulation class
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Nov 15, 2013
 */


#pragma once
#ifndef PDA_H_
#define PDA_H_

#include <set>
#include <map>
#include <string>
#include <stack>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <limits>
#include <sstream>
#include <stack>
#include "../TupleTriple/Triple.h"
#include "../TupleTriple/Tuple.h"
#include "../DesignByContract.h"
#include "../Parser/tinyxml2.h"
#include "../Verbose/Verbose.h"


namespace mnb {

//a class to keep track of the current state
class PDASimulation{
public:
	/*
	 * check if two states are the same
	 */
	inline bool operator == (const PDASimulation &b) const
	{
		return ( (b.stack == stack) &&
				(b.input==input) && (b.currentState == currentState));
	}
	/*
	 * check if two states are not the same
	 */
	inline bool operator != (const PDASimulation &b) const
			{
		return ( (b.stack != stack) ||
				(b.input!=input) || (b.currentState != currentState));
			}

	/*
	 * give for the current state a ID
	 * @return std::string the id of the furrent state
	 */
	std::string toString(){
		std::stringstream ss;
		ss<<"("<<currentState<<","<<stackToString(input)<<","<<stackToString(stack)<<")";
		return ss.str();
	}

	/*
	 * convert a stack to a string
	 * @param std::stack<std::string> stack the stack we want to convert
	 * @return std::String the string representation of the given stack
	 */
	std::string stackToString(std::stack<std::string> stack){
		std::stringstream ss;
		while(!stack.empty()){
			ss<<stack.top();
			stack.pop();
		}
		return ss.str();
	}

	//the information on the stack
	std::stack<std::string> stack;

	//the rest of the input
	std::stack<std::string> input;

	//the curent state we are in
	std::string currentState;



};

/*
 * a class to keep our ID's in a nice data structure
 */
class Node{
public:
	/*
	 * set the state of the pda
	 * @param PDASimulation  state the state the PDA is in
	 */
	void setState(PDASimulation  state){
		_state = state;
	}

	/*
	 * add a child to this state (the derivation of the current state)
	 * @param Node* n the node pointer to the child
	 */
	void addchild(Node* n){
		_childeren.push_back(n);
	}
	/*
	 * a getter for the childeren of the current state
	 */
	std::vector<Node*> childeren(){
		return _childeren;
	}

private:
	PDASimulation _state;
	std::vector<Node*> _childeren;
};

class PDA {
public:


	PDA(std::string fileName, bool verbose = false);


	PDA();

	virtual ~PDA();


	bool isPDA();

	void print();


	void printToDOT();


	void printToXML(std::string fileName, std::string path = "");


	bool simulate(std::string a);


	void transitionToTable();


	bool properlyInitialized();

	const std::set<std::string>& getE() const;
	void setE(const std::set<std::string>& e);
	const std::set<std::string>& getF() const;
	void setF(const std::set<std::string>& f);
	const std::set<std::string>& getQ() const;
	void setQ(const std::set<std::string>& q);
	const std::string& getQ0() const;
	void setQ0(const std::string& q0);
	const std::set<std::string>& getS() const;
	void setS(const std::set<std::string>& s);
	const std::vector<std::pair<Triple<std::string>, Tuple<std::string> > >& getT() const;
	void setT(
			const std::vector<
					std::pair<Triple<std::string>, Tuple<std::string> > >& t);
	const std::string& getZ0() const;
	void setZ0(const std::string& z0);

private:

	void parse(std::string& fileName);


	void readElements(tinyxml2::XMLNode* root);


	void readElement(tinyxml2::XMLNode* root);


	void readTransitie(tinyxml2::XMLNode* root);



	bool notEmpty();

	std::vector<PDASimulation> findChilderenOf(PDASimulation parent);




	mnb::Verbose* _verbose;
	PDA* InitCheck;

	//formal definition of the stack
	std::set<std::string> _Q;
	std::set<std::string> _E;
	std::set<std::string> _S;
	std::vector<std::pair<Triple<std::string>, Tuple<std::string> > > _T;
	std::string _q0;
	std::string _Z0;
	std::set<std::string> _F;
	int _maxStackSize;

	Node* _graph;




};



} /* namespace mnb */
#endif /* PDA_H_ */
