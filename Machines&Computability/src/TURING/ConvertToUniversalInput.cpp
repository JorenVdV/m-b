/**
 * @file ConvertToUniversalInput.cpp
 * @class mnb::ConvertToUniversalInput
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Jan 13, 2014
 */

#include "ConvertToUniversalInput.h"

namespace mnb {

/**
 * @brief the standard constructor for the convert to universal input class
 */
ConvertToUniversalInput::ConvertToUniversalInput() : _verbose(_verbose->instance(false)), _isPDA(false), InitCheck(this) {
	_verbose->addFunction("ConvertToUniversalInput::ConvertToUniversalInput()");
	ENSURE(this->properlyInitialized(), "the converter is not properlyInitialized");
	_verbose->closeCurrentFunction();
}

/**
 * @brief the standard destructor for the convert to universal input class
 */
ConvertToUniversalInput::~ConvertToUniversalInput() {
	// TODO Auto-generated destructor stub
}


/**
 * @brief a function to easily  create the desire tapes for the UTM out of a TM
 * @pre object must be properly initialized
 * @param Turning tm the turning machine that needs to be converted
 * @prarm string startState the start state of the TM
 * @param set<string> _acceptState the set of accepting states for the TM
 * @return the data tape and the program tape
 */
std::pair<Tape, Tape> ConvertToUniversalInput::turningToUniversalInput(Turing& tm, std::string& _startState, std::set<std::string>& _acceptState){
	_verbose->addFunction("std::pair<Tape, Tape> ConvertToUniversalInput::turningToUniversalInput(Turning& tm, std::string& _startState, std::set<std::string>& _acceptState)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");

	//it is not a pda that we current are converting
	_isPDA = false;

	//encode startState
	_startState = encodeState(tm.getS());

	//encode acceptingstates
	for(std::set<std::string>::iterator it = tm.getAs().begin(); it != tm.getAs().end(); ++it){
		_acceptState.insert(encodeState((*it)));
	}




	std::vector<std::pair<Tuple<std::string>, Triple<std::string> > > transitions = tm.getTransition();

	std::vector<std::string> encode;

	//encode the transitions
	for(unsigned int i = 0; i < transitions.size(); ++i){
		encodeTuple(transitions.at(i).first, encode);
		encode.push_back("0");
		encodeTriple(transitions.at(i).second, encode);
		encode.push_back("00");
	}

	Tape transition = Tape(encode);

	encode.clear();

	Tape tape = tm.getT();

	//encode the input
	tape.firstNode();
	while(tape.getData() != "B"){
		encode.push_back(encodeSymbol(tape.getData()));
		encode.push_back("0");
		tape.nextNode();
	}

	Tape input = Tape(encode);
	encode.clear();

	_verbose->closeCurrentFunction();

	//return the two tapes
	return std::make_pair(transition, input);



}

/**
 * @brief a function to easily  create the desire tapes for the UTM out of a PDA
 * @pre object must be properly initialized
 * @param PDA pda the pda that needs to be converted
 * @prarm string startState the start state of the PDA
 * @param set<string> _acceptState the set of accepting states for the PDA
 * @param string startStackSymbol the start stack symbol of the pda
 * @return the data tape and the program tape
 */
std::pair<Tape, Tape> ConvertToUniversalInput::PDAToUniversalInput(PDA& pda, std::string _input, std::string& _startState, std::set<std::string>& _acceptState, std::string& _startStackSymbol){
	_verbose->addFunction("std::pair<Tape, Tape> ConvertToUniversalInput::PDAToUniversalInput(PDA& pda, std::string _input, std::string& _startState, std::set<std::string>& _acceptState)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");

	//the machine that we are converting is a pda
	_isPDA = true;

	//encode start state
	_startState = encodeState(pda.getQ0());



	//encode acceptingstates
	_acceptState.insert(encodeState((*pda.getF().begin())));

	//encode startStackSymbol
	_startStackSymbol = encodeSymbol(pda.getZ0());




	std::vector<std::pair<Triple<std::string>, Tuple<std::string> > > transitions = pda.getT();

	std::vector<std::string> encode;
	std::stringstream temp;

	//encod the transitions
	for(unsigned int i = 0; i < transitions.size(); ++i){
		encodeTriple(transitions.at(i).first, encode);
		encode.push_back("0");
		encodeTuple(transitions.at(i).second, encode);
		if(i != (transitions.size() - 1))
			encode.push_back("00");
	}

	Tape transition = Tape(encode);

	encode.clear();

	//encode the start situation
	encode.push_back(_startState);
	encode.push_back("0");
	encode.push_back(encodeSymbol(_input));
	encode.push_back("0");
	encode.push_back(_startStackSymbol);

	Tape startSituation = Tape(encode);

	encode.clear();



	_verbose->closeCurrentFunction();
	//return the two tapes
	return std::make_pair(transition, startSituation);

}

/**
 * @brief a function to decode the datatape
 * @pre object must be properly initialized
 * @param shared_ptr<Tape> dataTape the data tape that should be decoded
 * @return the decode datatape in a string format
 */
std::string ConvertToUniversalInput::decodedataTape(std::shared_ptr<Tape> dataTape){
	_verbose->addFunction("ConvertToUniversalInput::decodedataTape(std::shared_ptr<Tape> dataTape)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	dataTape->firstNode();
	std::stringstream ss;
	ss.str(std::string());
	while(dataTape->getData() != "B"){
		if(dataTape->getData() != "0")
			ss<<decodeSymbol(dataTape->getData());
		else
			ss<<"-";
		dataTape->nextNode();
	}
	return ss.str();
	_verbose->closeCurrentFunction();
}


/**
 * @brief a function to quickly encode the tuple part of a transition
 * @pre object must be properly initialized
 * @param Tuple<string> input the tuple part of a transition
 * @param vector<string> encode the encoded tuple in a vector format
 */
void ConvertToUniversalInput::encodeTuple(Tuple<std::string> input, std::vector<std::string>& encode){
	_verbose->addFunction("std::string ConvertToUniversalInput::encodeTuple(Tuple<std::string> input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	encode.push_back(encodeState(input.first()));
	encode.push_back("0");
	encode.push_back(encodeSymbol(input.second()));
	_verbose->closeCurrentFunction();

}

/**
 * @brief a function to quickly encode the triple part of a transition
 * @pre object must be properly initialized
 * @param Triple<string> input the tuple part of a transition
 * @param vector<string> the encoded  vector
 */
void ConvertToUniversalInput::encodeTriple(Triple<std::string> input, std::vector<std::string>& encode){
	_verbose->addFunction("std::string ConvertToUniversalInput::encodeTriple(Triple<std::string> input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	encode.push_back(encodeState(input.first()));
	encode.push_back("0");
	encode.push_back(encodeSymbol(input.second()));
	encode.push_back("0");
	if(_isPDA)
		encode.push_back(encodeSymbol(input.thirth()));
	else
		encode.push_back(encodeMove(input.thirth()));
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to encode the symbol
 * @pre object must be properly initialized
 * @param string input the string that should be encoded
 * @return the encoded string
 */
std::string ConvertToUniversalInput::encodeSymbol(std::string input){
	_verbose->addFunction("std::string ConvertToUniversalInput::encodeSymbol(std::string input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	std::stringstream ss;
	ss.str(std::string());
	if(input.size() == 1){
		char symbol = (*input.begin());
		int intSymbol = symbol;
		int index;

		std::string code;

		if( (intSymbol > 64) && (intSymbol < 91) ){
			index = intSymbol - 64;
			code = "1";
		} else if((intSymbol > 96) && (intSymbol < 123)){
			index = intSymbol - 96;
			code = "2";
		} else if((intSymbol > 47) && (intSymbol < 58)){
			index = intSymbol - 47;
			code = "3";
		} else{
			index = intSymbol - 33;
			code = "4";
		}

		for(int i = 0 ; i < index; ++i){
			ss<<code;
		}
	}
	else{
		for(unsigned int i = 0; i < input.size(); ++i){
			char symbol = input.at(i);
			int intSymbol = symbol;
			int index;

			std::string code;

			if( (intSymbol > 64) && (intSymbol < 91) ){
				index = intSymbol - 64;
				code = "1";
			} else if((intSymbol > 96) && (intSymbol < 123)){
				index = intSymbol - 96;
				code = "2";
			} else if((intSymbol > 47) && (intSymbol < 58)){
				index = intSymbol - 47;
				code = "3";
			}else{
				index = intSymbol - 33;
				code = "4";
			}

			for(int i = 0 ; i < index; ++i){
				ss<<code;
			}
			if( i < (input.size() - 1) )
				ss<<"000";
		}
	}
	_verbose->closeCurrentFunction();
	return ss.str();


}

/**
 * @brief a function to decode the symbol
 * @pre object must be properly initialized
 * @param string input the string that should be decoded
 * @return the decoded string
 */
std::string ConvertToUniversalInput::decodeSymbol(std::string input){
	_verbose->addFunction("std::string ConvertToUniversalInput::decodeSymbol(std::string input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	std::stringstream tokens;
	int index;
	int intSymbol;
	char symbol;
	if(input.find("000") ==  std::string::npos){
		index =  input.size();
		intSymbol = 0;

		if((*input.begin()) == '1'){
			intSymbol = index + 64;
		}else if((*input.begin()) == '2'){
			intSymbol = index + 96;
		}else if((*input.begin()) == '3'){
			intSymbol = index + 47;
		}else{
			intSymbol = index + 33;
		}

		symbol = intSymbol;
		tokens<<symbol;;
	}else{
		int i = 0;
		std::stringstream stream;
		while(i < (input.size())){
			while((i < input.size() &&(input.at(i) != '0'))){
				stream<<input.at(i);
				i++;
			}
			index =  stream.str().size();
			intSymbol = 0;

			if((*stream.str().begin()) == '1'){
				intSymbol = index + 64;
			}else if((*stream.str().begin()) == '2'){
				intSymbol = index + 96;
			}else if((*stream.str().begin()) == '3'){
				intSymbol = index + 47;
			}
			else{
				intSymbol = index + 33;
			}
			symbol = intSymbol;

			tokens<<symbol;
			i += 3;
			stream.str(std::string());

		}
	}

	_verbose->closeCurrentFunction();
	return tokens.str();


}

/**
 * @brief a function to encode the state
 * @pre object must be properly initialized
 * @param string input the string that should be encoded
 * @return the encoded string
 */
std::string ConvertToUniversalInput::encodeState(std::string input){
	_verbose->addFunction("std::string ConvertToUniversalInput::encodeState(std::string input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");


	if(contains_number(input.c_str())){
		int index = atoi(input.substr(input.size() - 1).c_str());

		std::stringstream ss;
		ss.str(std::string());

		for(int i = -1 ; i < index; ++i){
			ss<<"1";
		}
		return ss.str();
	}else{
		_verbose->closeCurrentFunction();
		return encodeSymbol(input);
	}

}

/**
 * @brief a function to decode the state
 * @pre object must be properly initialized
 * @param string input the string that should be decoded
 * @return the decoded string
 */
std::string ConvertToUniversalInput::decodeState(std::string input){
	_verbose->addFunction("std::string ConvertToUniversalInput::decodeState(std::string input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	if((*input.begin()) == '1'){
		int index = input.size() - 1;

		std::stringstream ss;
		ss.str(std::string());

		ss<<"q"<<index;

		_verbose->closeCurrentFunction();
		return ss.str();
	}else{
		_verbose->closeCurrentFunction();
		return decodeSymbol(input);

	}
}

/**
 * @brief a function to encode the move
 * @pre object must be properly initialized
 * @param string input the string that should be encoded
 * @return the encoded string
 */
std::string ConvertToUniversalInput::encodeMove(std::string input){
	_verbose->addFunction("std::string ConvertToUniversalInput::encodeMove(std::string input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	if(input == "L")
		return "1";
	else if(input == "R")
		return "11";
	else
		return "0";
	return std::string();
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to decode the move
 * @pre object must be properly initialized
 * @param string input the string that should be decoded
 * @return the decoded string
 */
std::string ConvertToUniversalInput::decodeMove(std::string input){
	_verbose->addFunction("std::string ConvertToUniversalInput::decodeMove(std::string input)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	if(input == "1")
		return "L";
	else if(input == "11")
		return "R";
	else
		return "0";
	return std::string();
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to check if a given string contains any numbers
 * @pre object must be properly initialized
 * @param string c the string that may contains nummbers
 * @return true if it contains a number false if not.
 */
bool ConvertToUniversalInput::contains_number(const std::string &c)
{
	_verbose->addFunction("ConvertToUniversalInput::contains_number(const std::string &c)");
	REQUIRE(this->properlyInitialized(), "CUTM is not properly initialized");
	return (c.find_first_of("0123456789") != std::string::npos);
	_verbose->closeCurrentFunction();
}

/**
 * @brief validation for postcondition
 * @return true if it is properly initialized, if not than false
 */
bool ConvertToUniversalInput::properlyInitialized(){
	return (this == InitCheck);
}




} /* namespace mnb */
