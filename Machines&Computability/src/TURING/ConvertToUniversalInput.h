/**
 * @file ConvertToUniversalInput.h
 * @class mnb::ConvertToUniversalInput
 * @brief converts a TM or PDA to input for a UTM
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Jan 13, 2014
 */

#pragma once
#ifndef CONVERTTOUNIVERSALINPUT_H_
#define CONVERTTOUNIVERSALINPUT_H_

#include <string>
#include <sstream>
#include "Turing.h"
#include "../PDA/PDA.h"
#include <set>
#include <vector>
#include "../TupleTriple/Triple.h"
#include "../TupleTriple/Tuple.h"
#include "../Verbose/Verbose.h"

namespace mnb {

class ConvertToUniversalInput {
public:
	ConvertToUniversalInput();
	virtual ~ConvertToUniversalInput();

	std::pair<Tape, Tape> turningToUniversalInput(Turing& tm,std::string& _startState, std::set<std::string>& _acceptState);
	std::pair<Tape, Tape> PDAToUniversalInput(PDA& pda, std::string _input, std::string& _startState, std::set<std::string>& _acceptState,  std::string& _startStackSymbol);
	std::string decodedataTape(std::shared_ptr<Tape> dataTape);
	void encodeTuple(Tuple<std::string> input, std::vector<std::string>& encode);
	void encodeTriple(Triple<std::string> input, std::vector<std::string>& encode);
	std::string encodeSymbol(std::string input);
	std::string decodeSymbol(std::string input);
	std::string encodeState(std::string input);
	std::string decodeState(std::string input);
	std::string encodeMove(std::string input);
	std::string decodeMove(std::string input);
	bool contains_number(const std::string &c);
	bool properlyInitialized();

private:
	Verbose* _verbose;
	bool _isPDA;
	ConvertToUniversalInput* InitCheck;


};
} /* namespace mnb */

#endif /* CONVERTTOUNIVERSALINPUT_H_ */
