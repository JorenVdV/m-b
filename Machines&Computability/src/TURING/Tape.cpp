/**
 * @file Tape.cpp
 * @class mnb::Tape
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *
 *  Created on: Dec 16, 2013
 */

#include "Tape.h"

namespace mnb {

/**
 * @brief constructor for class Tape
 * @param std::vector<std::string> v, all entries for the tape nodes
 * @param bool verb dictates the use of the verbose tracking
 * @post object must be properly initialized
 */
Tape::Tape(std::vector<std::string> v, bool verb) :
			_verbose(_verbose->instance(verb)),
			_initCheck(this){

	std::shared_ptr<NODE> prev (new NODE);
	prev->_data = "B";
	for(unsigned int i = 0; i < v.size(); ++i){
		std::shared_ptr<NODE> n (new NODE);
		n->_prev = prev;
		n->_data = v.at(i);
		if(i == 0){
			_currentNode = n;
			_firstNode = n;
		}else if(i == (v.size() - 1)){
			_lastNode = n;
		}
		prev->_next = n;
		prev = n;
	}
	ENSURE(properlyInitialized(),
			"Object not well defined : Tape");
}

/**
 * @brief constructor for class Tape
 * @param std::string s, string containing all entries for the Tape
 * @param bool verb dictates the use of the verbose tracking
 * @post object must be properly initialized
 */
Tape::Tape(std::string s, bool verb) :
		_verbose(_verbose->instance(verb)),
		_initCheck(this){

	std::shared_ptr<NODE> prev (new NODE);
	prev->_data = "B";
	std::stringstream ss;
	for(unsigned int i = 0; i < s.size(); ++i){
		std::shared_ptr<NODE> n (new NODE);
		n->_prev = prev;
		ss<<s.at(i);
		n->_data = ss.str();
		if(i == 0){
			_currentNode = n;
			_firstNode = n;
		}else if(i == (s.size() - 1)){
			_lastNode = n;
		}
		prev->_next = n;
		prev = n;
		ss.str(std::string());
	}
	ENSURE(properlyInitialized(),
			"Object not well defined : Tape");

}

/**
 * @brief constructor for class Tape
 * @param bool verb dictates the use of the verbose tracking
 * @post object must be properly initialized
 */
Tape::Tape(bool verbose):
	_verbose(_verbose->instance(verbose)),
	_initCheck(this){
	std::shared_ptr<NODE> n (new NODE);
	_currentNode = n;
	_firstNode = n;
	_lastNode = n;
	n->_data = "B";
	n->_prev = NULL;
	n->_next = NULL;
	ENSURE(properlyInitialized(),
				"Object not well defined : Tape");
}

/**
 * @brief destructor for class Tape
 */
Tape::~Tape() {
	//find first Node
	_currentNode = _firstNode;
	while (_currentNode != NULL){
		std::shared_ptr<NODE> temp = _currentNode;
		_currentNode = _currentNode->_next;
		temp.reset();
	}

}

/**
 * @brief takes the next node on the tape,
 * 		sets _currentpointer to next element
 * @pre object must be properly initialized
 */
void Tape::nextNode(){
	_verbose->addFunction("TAPE::nextNode()");
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	if (_currentNode->_next == NULL){
		_verbose->comment("next node did not yet exist, constructing new blank node");
		std::shared_ptr<NODE> newNode (new NODE);
		newNode->_prev = _currentNode;
		newNode->_next = NULL;
		newNode->_data = "B";
		_currentNode->_next = newNode;
		_lastNode = newNode;
	}
	_currentNode = _currentNode->_next;
	_verbose->closeCurrentFunction();

}

/**
 * @brief takes the previous node on the tape,
 * 		sets _currentNode to previous element
 * @pre object mus be properly initialized
 */
void Tape::prevNode(){
	_verbose->addFunction("TAPE::prevNode()");
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	if (_currentNode->_prev == NULL){
		_verbose->comment("previous node did not yet exist, constructing new blank node");
		std::shared_ptr<NODE> newNode (new NODE);
		newNode->_prev = NULL;
		newNode->_next = _currentNode;
		newNode->_data = "B";
		_currentNode->_prev = newNode;
		_firstNode = newNode;

	}
	_currentNode = _currentNode->_prev;
	_verbose->closeCurrentFunction();
}

/**
 *	@brief sets the _currentNode to the first element
 *	@pre object must be properly initialized
 */
void Tape::firstNode(){
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	_currentNode = _firstNode;
}

/**
 *	@brief sets the _currentNode to the last element
 *	@pre object must be properly initialized
 */
void Tape::lastNode(){
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	_currentNode = _lastNode;
}

/**
 * @brief gets the data of the currentNode
 * @return std::string, the data of the current node
 * @pre object must be properly initialized
 */
std::string Tape::getData(){
	_verbose->addFunction("TAPE::getData()");
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	_verbose->closeCurrentFunction();
	return _currentNode->_data;

}
void Tape::setData(std::string s){
	_verbose->addFunction("TAPE::setData(std::string s)");
	_currentNode->_data = s;
	_verbose->closeCurrentFunction();
}

/**
 * @brief prints the content of the tape to terminal
 * @pre object must be properly initialized
 */
void Tape::print(){
	_verbose->addFunction("Tape::print()");
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	std::shared_ptr<NODE> temp = _currentNode;

	firstNode();
	while(getData() != "B"){
		std::cout<<getData();
		nextNode();
	}
	std::cout<<"\n";

	_currentNode = temp;
	_verbose->closeCurrentFunction();
}

/**
 * @brief get the current node in the tape
 * @return Node, the current node
 */
const std::shared_ptr<NODE>& Tape::getCurrentNode() const {
	return _currentNode;
}

/**
 * @brief sets the current node to given node
 * @param the node to wich currentnode will be set
 * @pre object must be properly initialized
 */
void Tape::setCurrentNode(const std::shared_ptr<NODE>& currentNode) {
	REQUIRE(properlyInitialized(),
			"Object not well formed : Tape");
	_currentNode = currentNode;
}

bool Tape::properlyInitialized(){
	return (this == _initCheck)?true:true;
}
} /* namespace si */
