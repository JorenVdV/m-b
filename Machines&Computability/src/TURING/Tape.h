/*
 * @file Tape.h
 * @class mnb::Tape
 * @brief
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *
 *  Created on: Dec 16, 2013
 */

#pragma once
#ifndef TAPE_H_
#define TAPE_H_

#include <string>
#include <vector>
#include "../DesignByContract.h"
#include "../Parser/tinyxml2.h"
#include "../Verbose/Verbose.h"
#include <memory>

namespace mnb {

struct NODE{
	std::shared_ptr<NODE> _next;
	std::shared_ptr<NODE> _prev;
	std::string _data;

}; /* struct NODE */


class Tape {
	/**
	 * tape class for turing machines
	 * will be initialized as a linked list of x nodes
	 * and can grow infinitely
	 */
public:
	Tape( std::vector< std::string > v , bool verbose= false);
	Tape(std::string v, bool verb = false);
	Tape(bool verbose= false);

	virtual ~Tape();

	void nextNode();
	void prevNode();
	void firstNode();
	void lastNode();

	std::string getData();
	void setData(std::string s);

	void print();
	const std::shared_ptr<NODE>& getCurrentNode() const;
	void setCurrentNode(const std::shared_ptr<NODE>& currentNode);

	bool properlyInitialized();
private:
	mnb::Verbose* _verbose;
	Tape* _initCheck;

	std::shared_ptr<NODE> _firstNode;
	std::shared_ptr<NODE> _lastNode;
	std::shared_ptr<NODE>  _currentNode;
}; /*class TAPE */



} /* namespace si */

#endif /* TAPE_H_ */
