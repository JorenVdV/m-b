/**
 * @file Turing.cpp
 * @class mnb::Turing
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *
 *  Created on: Dec 16, 2013
 */

#include "Turing.h"

namespace mnb {


/**
 * @brief constructs a Turing from a given XML-file adds a verbose mode to the functions
 * @param string fileName, path and name of XML-file to parse
 * @param bool verbose, gives additional output for debugging, standard set to false.
 * @post object must be properly initialized
 * @see properlyInitialized()
 */
Turing::Turing(std::string& fileName, bool verb):
	_verbose(_verbose->instance(verb)),
	InitCheck(this),
	tapeset(false){
	_verbose->addFunction("Turing::Turing(std::string& fileName, bool verb)");

	parse(fileName);
	//_T = Tape("");

	ENSURE(properlyInitialized(),
			"Object could not be created: Turing");
	_verbose->closeCurrentFunction();
}

/**
 * @brief Destructor for class Turing
 */
Turing::~Turing() {
	_verbose->addFunction("Turing::~Turing()");
	_verbose->closeCurrentFunction();
}

/**
 * @brief parse the xml so that is become a PDA
 * @pre object must be properly initialized
 * @pre file must exists.
 * @see properlyInitialized()
 * @pre the file is valid xml file and a valid path
 * @post nothing may be empty empty
 */
void Turing::parse(std::string& fileName){
	_verbose->addFunction("Turing::parse(std::string& fileName)");
	REQUIRE(properlyInitialized(), "Object Turing not well defined");
	try{
		tinyxml2::XMLDocument doc;
		//check if file exists
		REQUIRE(doc.fileExists(fileName), "file does not exists");
		//load xml file
		doc.LoadFile(fileName.c_str());

		//temp data for correct verbose output
		std::string elementName;
		std::string message;

		//search the root node
		tinyxml2::XMLNode *rootNode = doc.FirstChild();
		if(!rootNode) throw "There was no root node found.";
		elementName = std::string(rootNode->Value());
		message = "Found the root node: <" + elementName + ">";
		_verbose->comment(message);

		//search the sibling after the root
		tinyxml2::XMLNode* siblingNode = rootNode ->NextSibling();
		if(!siblingNode) throw "there is no siblingNode for the rootNode";
		elementName = std::string(siblingNode->Value());
		message = "Found the sibling node: <" + elementName + ">";
		_verbose->comment(message);

		tinyxml2::XMLNode* childNode = siblingNode ->FirstChild();
		if(!childNode) throw "there is no childNode for <Turing>";

		while(childNode){
			//check if child of the Turing element has children of his own
			//if yes than we need to add multiple elements else we need to add just one
			if(childNode->FirstChild()->FirstChild() == NULL){
				readElement(childNode);
			}
			else{
				readElements(childNode);
			}
			//go to the next child of the pda element
			childNode = childNode->NextSibling();
		}
		ENSURE(this->notEmpty(), "there is still a variable that is empty");


	}catch(const char* err){
		_verbose->error(err);;
		exit (EXIT_FAILURE);
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to read a element with mulitiply childeren.
 * @param tinyxml2::XMLNode* root the root of the current location in the xml.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @throw "Encountered a unrecognizable element";
 */
void Turing::readElements(tinyxml2::XMLNode* root){
	_verbose->addFunction("Turing::readElements(tinyxml2::XMLNode* root)");
	REQUIRE(properlyInitialized(), "Object Turing was not well defined");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";
	//get the name of the current element
	std::string elementName = root->Value();
	//pre-initialized data so that it does not need to be initialized in the while loop
	tinyxml2::XMLNode* childNode = root->FirstChild();
	tinyxml2::XMLElement *childElmtl;
	std::string data;
	std::string message;
	//loop over the child elements of the current element, as long as there are siblings
	while(childNode){
		if(elementName != "Transitions"){
			childElmtl = childNode->ToElement();
			//the data of this element
			data = std::string(childElmtl->GetText());
			if(elementName == "States"){
				message = "Add " + data + " to States";
				_verbose->comment(message);
				_Q.insert(data);
			} else if(elementName == "AcceptingStates"){
				message = "Add " + data + " to AcceptingStates";
				_verbose->comment(message);
				_AS.insert(data);
			} else if(elementName == "TapeAlphabet"){
				message = "Add " + data + " to TapeAlphabet";
				_verbose->comment(message);
				_TA.insert(data);
			} else if(elementName == "InputAlphabet"){
				message = "Add " + data + " to InputAlphabet";
				_verbose->comment(message);
				_E.insert(data);
			}else{
				throw "Encountered a unrecognizable element";
			}
		} else if(elementName == "Transitions"){
			readTransitie(childNode);

		}
		//get the next sibling of the current element
		childNode = childNode->NextSibling();
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to read a element with just one child.
 * @param tinyxml2::XMLNode* root the root of the current location in the xml.
 * @pre object must be properly initialized
 * @throw root pointer is null
 * @throw "Encountered a unrecognizable element";
 */
void Turing::readElement(tinyxml2::XMLNode* root){
	_verbose->addFunction("Turing::readElement(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "Object Turing not well defined");
	//check if the root pointer is not null
	if(!root) throw "root pointer is null";
	//get the name of the current element
	std::string elementName = root->Value();
	//get the first child of the current element
	tinyxml2::XMLNode* childNode = root->FirstChild();
	//temp data for correct verbose output
	std::string data;
	std::string message;
	//the data of this element
	data = std::string(childNode->Value());
	//add the the data to the object variable
	if(elementName == "StartState"){
		message = "Add " + data + " to Start state";
		_verbose->comment(message);
		_S = data;
	}else if(elementName == "BlankSymbol"){
		message = "Add " + data + " to Blank";
		_verbose->comment(message);
		_B = data;
	}else{
		throw "Encountered a unrecognizable element";
	}
}

/**
 * @brief a function to read the transition elements
 * @param tinyxml2::XMLNode* root the root of the current location in the xml.
 * @pre object must be properly initialized
 * @throw root pointer is null
 */
void Turing::readTransitie(tinyxml2::XMLNode* root){
	_verbose->addFunction("PDA::readTransitie(tinyxml2::XMLNode* root)");
	REQUIRE(this->properlyInitialized(), "transitie could not be parsed, not properly initialized");
	//check if the root node is null
	if(!root) throw "root pointer is null";
	//pre-initialized data
	tinyxml2::XMLNode* childNode = root->FirstChild();
	tinyxml2::XMLNode* childSiblNode = childNode->NextSibling();
	childNode = childNode->FirstChild();
	childSiblNode = childSiblNode->FirstChild();
	tinyxml2::XMLElement *childElmtl;
	tinyxml2::XMLElement *childSiblElmtl;
	std::string data;
	std::string message;
	Triple<std::string> triple;
	Tuple<std::string> tuple;
	//parse erything from lt and put it in the triple.
	while(childNode){
		childElmtl = childNode->ToElement();
		data = std::string(childElmtl->GetText());
		tuple.add(data);
		childNode = childNode->NextSibling();
	}
	//parse everything from the rt and put it in the tuple
	while(childSiblNode){
		childSiblElmtl = childSiblNode->ToElement();
		data = std::string(childSiblElmtl->GetText());
		triple.add(data);
		childSiblNode = childSiblNode ->NextSibling();
	}
	message = "Add " + tuple.toString() + "-->" + triple.toString() + " to Transition";
	_verbose->comment(message);
	//add the the data to the object variable
	_transition.push_back(std::make_pair(tuple, triple));
	triple.clear();
	tuple.clear();
	_verbose->closeCurrentFunction();

}

/*
 * @brief helper function for class Turing
 * 		check all datamembers on emptyness
 * @return bool whether or not one of the members is empty
 * @pre object must be properly initialized
 */
bool Turing::notEmpty(){
	_verbose->addFunction("Turing::notEmpty()");
	REQUIRE(properlyInitialized(), "Object Turing not well defined");
	if (_Q.empty()){
		_verbose->error("Q is empty");
		_verbose->closeCurrentFunction();
		return false;
	} else if (_TA.empty()){
		_verbose->error("TA is empty");
		_verbose->closeCurrentFunction();
		return false;
	} else if (_E.empty()){
		_verbose->error("E is empty");
		_verbose->closeCurrentFunction();
		return false;
	} else if (_AS.empty()){
		_verbose->error("AS is empty");
		_verbose->closeCurrentFunction();
		return false;
	} else if (_transition.empty()){
		_verbose->error("Transition is empty");
		_verbose->closeCurrentFunction();
		return false;
	} else if (_B == ""){
		_verbose->error("Blank is empty");
		_verbose->closeCurrentFunction();
		return false;
	} else if (_S == ""){
		_verbose->error("Startstate is empty");
		_verbose->closeCurrentFunction();
		return false;
	}
	_verbose->closeCurrentFunction();
	return true;
}

/**
 * @brief prints the TM to a terminal representation
 * @pre object must be properly initialized
 */
void Turing::print(){
	_verbose->addFunction("Turing::print()");
	REQUIRE(properlyInitialized(), "Object Turing not well defined");
	std::cout<<"=================Turing================"<<std::endl;
	_verbose->comment("Printing states");
	std::cout<<"States = {";
	std::set<std::string>::iterator itQ = _Q.begin();
	std::cout<<*itQ++;
	for(;itQ != _Q.end(); itQ++)
		std::cout<<", "<<*itQ;
	std::cout<<"}"<<std::endl;

	_verbose->comment("Printing Tape alphabet");
	std::cout<<"Tape alphabet = {";
	itQ = _TA.begin();
	std::cout<<*itQ++;
	for(;itQ != _TA.end(); itQ++)
		std::cout<<", "<<*itQ;
	std::cout<<"}"<<std::endl;

	_verbose->comment("Printing Blank symbol");
	std::cout<<"Blank symbol = "<<_B<<std::endl;

	_verbose->comment("Printing Input symbols");
	std::cout<<"Input symbols = {";
	itQ = _E.begin();
	std::cout<<*itQ++;
	for(;itQ != _E.end(); itQ++)
		std::cout<<", "<<*itQ;
	std::cout<<"}"<<std::endl;

	_verbose->comment("Printing Startstate");
	std::cout<<"Startstate = "<<_S<<std::endl;

	_verbose->comment("Printing Accepting states");
	std::cout<<"Accepting states = {";
	itQ = _AS.begin();
	std::cout<<*itQ++;
	for(;itQ != _AS.end(); itQ++)
		std::cout<<", "<<*itQ;
	std::cout<<"}"<<std::endl;

	_verbose->comment("Printing Transition");
	std::cout<<"Transitions = {"<<std::endl;
	for(auto it:_transition)
		std::cout<<"	"<<it.first.toString()<< "-->" << it.second.toString()<<std::endl;
	std::cout<<"	}"<<std::endl;

	if(tapeset){
		_verbose->comment("Printing Tape");
		std::cout<<"Tape = ";
		_T.print();
	}
	std::cout<<"======================================="<<std::endl;

	_verbose->closeCurrentFunction();
}

/**
 * @brief simulates the turing machine
 * @returns the tape after execution
 * @pre object must be properly initialized
 */
Tape Turing::simulate(){
	_verbose->addFunction("std::string Turing::simulate()");
	REQUIRE(properlyInitialized(),
			"Object not well defined: Turing");
	// variables needed for runtime
	try{
		bool accept = false;
		std::string currentstate = _S;
		std::string currentdata = "";
		Triple<std::string> currenttrans;
		// main loop
		while(!accept){

			//get data
			currentdata = _T.getData();
			bool found = false;
			for (auto i: _transition){
				if(i.first.first() == currentstate && i.first.second() == currentdata){
					currenttrans = i.second;
					found = true;
				}
			}
			if(!found)throw "Coudn't find any transitions matching your state ";
			currentstate = currenttrans.first();
			_T.setData(currenttrans.second());
			if(currenttrans.thirth() == "R") _T.nextNode();
			else if(currenttrans.thirth() == "L") _T.prevNode();
			else throw "invalid move";

			// if state is an end state
			if(_AS.find(currentstate) != _AS.end())accept = true;
		}
		std::cout<<"Simulation completed";
		if(accept) std::cout<<", input is accepted";
		std::cout<<std::endl;
	}catch(const char* err){
		_verbose->error(err);
		exit (EXIT_FAILURE);
	}

	_verbose->closeCurrentFunction();
	return _T;
}


} /* namespace mnb */
