/**
 * @file Turing.h
 * @class mnb::Turing
 * @brief Class for all Turing simulation
 * 		Turing machine with single tape
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *
 *  Created on: Dec 16, 2013
 */

#ifndef TURING_H_
#define TURING_H_

#include "Tape.h"
#include "../DesignByContract.h"
#include "../Parser/tinyxml2.h"
#include "../Verbose/Verbose.h"
#include "../TupleTriple/Triple.h"
#include "../TupleTriple/Tuple.h"
#include <set>
#include <string>

namespace mnb {

class Turing {
public:
	Turing(std::string& fileName, bool verb = false);
	virtual ~Turing();

	const std::vector<std::pair<Tuple<std::string>, Triple<std::string> > >& getTransition() const {return _transition;}
	const Tape& getT() const {return _T;}
	const std::string& getS() const {return _S;}
	const std::set<std::string>& getAs() const {return _AS;}
	const std::string& getB() const {return _B;}
	const std::set<std::string>& getE() const {return _E;}
	const std::set<std::string>& getQ() const {return _Q;}
	const std::set<std::string>& getTa() const { return _TA; }

	void setAs(const std::set<std::string>& as) {_AS = as;}
	void setB(const std::string& b = "B") {_B = b;}
	void setE(const std::set<std::string>& e) {_E = e;}
	void setQ(const std::set<std::string>& q) {_Q = q;}
	void setS(const std::string& s) {_S = s;}
	void setT(const Tape& t) {_T = t; tapeset=true;}
	void setTa(const std::set<std::string>& ta) { _TA = ta; }
	void setTransition(const std::vector<std::pair<Tuple<std::string>,
			Triple<std::string> > >& transition){
		_transition = transition;	}

	bool properlyInitialized(){ return (this == InitCheck); }

	void print();
	Tape simulate();


private:
	// private function
	void parse(std::string& fileName);
	void readElements(tinyxml2::XMLNode* root);
	void readElement(tinyxml2::XMLNode* root);
	void readTransitie(tinyxml2::XMLNode* root);
	bool notEmpty();

	//private members
	mnb::Verbose* _verbose;
	Turing* InitCheck;

	std::set<std::string> _Q;	//set of states
	std::set<std::string> _TA;	//t.at(0)ape alphabet
	std::set<std::string> _E;	//set of inputsymbols
	std::string _B = "B";		//blank symbol of stack
	std::string _S; 			//startstate
	std::set<std::string> _AS;  //accepting states
	std::vector<std::pair<Tuple<std::string>, Triple<std::string> > > _transition;

	Tape _T;		//tape
	bool tapeset;


};

} /* namespace si */

#endif /* TURING_H_ */
