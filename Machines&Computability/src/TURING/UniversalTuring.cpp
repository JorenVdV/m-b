/**
 * @file UniversalTuring.cpp
 * @class mnb::UniversalTuring
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Jan 13, 2014
 */

#include "UniversalTuring.h"

namespace mnb {

/**
 * @brief the constructor for the UTM for creating a UTM instance of a PDA
 * @param PDA pda the pda we want to simulate
 * @param string input the input for the string
 * @post the object must be properly initialized.
 */
UniversalTuring::UniversalTuring(PDA pda, std::string input) : _accept(false), _verbose(_verbose->instance(false)), _simulateTM(false), InitCheck(this){
	_verbose->addFunction("UniversalTurning::UniversalTurning(PDA pda, std::string input)");
	std::string startSituation;
	//create a encoded epsilon for easy usage
	_epsilon = _converter.encodeSymbol("e");

	//create the input for the UTM out of the PDA
	std::pair<Tape, Tape> tapes = _converter.PDAToUniversalInput(pda, input, _beginState, _acceptSates,  _startStackSymbol);

	//set all the tapes
	_programTape = std::make_shared<Tape>( Tape(tapes.first));


	_situationTape = std::make_shared<Tape>( Tape(tapes.second));

	_maxStackSize = 250;

	_programTape->firstNode();

	_situationTape->firstNode();

	_verbose->closeCurrentFunction();
	ENSURE(this->properlyInitialized(), "the UTM is not properlyInitialized");
}

/**
 * @brief the constructor for the UTM for creating a UTM instance of a TM
 * @param Turing tm the TM we want to simulate
 * @post the object must be properly initialized.
 */
UniversalTuring::UniversalTuring(Turing tm) : _accept(false), _verbose(_verbose->instance(false)),_simulateTM(true), _maxStackSize(0), InitCheck(this) {
	_verbose->addFunction("UniversalTurning::UniversalTurning(Turning tm)");

	//create the input for the UTM out of the TM
	std::pair<Tape, Tape> tapes = _converter.turningToUniversalInput(tm, _beginState, _acceptSates);

	//set all the states
	_programTape = std::make_shared<Tape>( Tape(tapes.first));
	_dataTape = std::make_shared<Tape>( Tape(tapes.second));
	std::vector<std::string> temp;
	temp.push_back(_beginState);
	_stateTape = std::make_shared<Tape>(  Tape(temp));


	_programTape->firstNode();
	_dataTape->firstNode();
	_stateTape->firstNode();


	_verbose->closeCurrentFunction();
	ENSURE(this->properlyInitialized(), "the UTM is not properlyInitialized");

}


/**
 * @brief the default destructor
 */
UniversalTuring::~UniversalTuring() {
	// TODO Auto-generated destructor stub
}


/**
 * @brief the function that simulates the utm (this function is for easy acces).
 * @pre object must be properly initialized
 */
void UniversalTuring::simulate(){
	_verbose->addFunction("UniversalTuring::simulate()");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	if(_simulateTM){
		simulateTM();
	}
	else{
		simulatePDA();
	}
	if(_accept){
		std::cout<<"the simulation is completed and the input is accepted"<<std::endl;
	}else{
		std::cout<<"the simulation is aborted and the input is not accepted"<<std::endl;
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief validation for postcondition
 * @return true if it is properly initialized, if not than false
 */
bool UniversalTuring::properlyInitialized(){
	return (this == InitCheck);
}


/**
 * @brief function to simulate the tm instance of the UTM
 * @pre object must be properly initialized
 */
void UniversalTuring::simulateTM(){
	_verbose->addFunction("UniversalTuring::simulateTM()");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::vector<std::string> instruction;
	//as long as it is not accepted we need to continue
	while(!_accept){
		instruction = findTMInstruction();
		//if the instruction is empty we can quit because we are stuck
		if(!instruction.empty()){
			//if we are going to be in  a accept state than we can quit
			if(_acceptSates.find(instruction.at(2)) != _acceptSates.end()){
				_accept = true;
			}
			//set the new data
			_dataTape->setData(instruction.at(3));
			//check if we need to go to the left or the right;
			if(instruction.at(4) == "11"){
				_dataTape->nextNode();
				while(_dataTape->getData() == "0"){
					_dataTape->nextNode();
				}
			}
			else{
				_dataTape->prevNode();
				while(_dataTape->getData() == "0"){
					_dataTape->prevNode();
				}
			}
			//set the info in the new tape
			_stateTape->nextNode();
			_stateTape->setData("0");
			_stateTape->nextNode();
			_stateTape->setData(instruction.at(2));

		}else{
			//stuck there is no instruction for the current situation sow the input is not accepted and we can leave the while loop
			break;
		}
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief function to simulate the pda instance of the UTM
 * @pre object must be properly initialized
 */
void UniversalTuring::simulatePDA(){
	_verbose->addFunction("UniversalTuring::simulatePDA()");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::vector<std::vector<std::string> > instructions;
	std::vector<std::string> instruction;
	std::vector<std::string> situation;
	std::stringstream topInput;
	std::stringstream topStack;
	std::shared_ptr<NODE> currentSituation;
	//as long as there are situations than we need to continu
	while(_situationTape->getData() != "B"){
		//get the situation we are in
		while((_situationTape->getData() != "00") && (_situationTape->getData() != "B")){

			if(_situationTape->getData() != "0"){
				situation.push_back(_situationTape->getData());
			}
			_situationTape->nextNode();

		}
		//creates the top of the top strings
		for(auto it = situation.at(1).begin(); it != situation.at(1).end(); ++it){
			if((*it) != '0'){
				topInput <<(*it);
			}
			else
				break;
		}
		for(auto it = situation.at(2).begin(); it != situation.at(2).end(); ++it){
			if((*it) != '0'){
				topStack <<(*it);
			}
			else
				break;
		}
		//search al the instuctions we can have for the current situation
		instructions = findPDAInstruction(situation.at(0), topStack.str(), topInput.str());
		//if the instructions are empty than we can check if we are in a accepting state or not
		//if we are in accepting state than we can quit else we need to remove the current situation because this one is usseless
		//if the instructions are not empty than we need to update the situation tape for every instruction
		if(!instructions.empty()){
			//set the current position of the tape so that the data will be the same
			currentSituation = _situationTape->getCurrentNode();
			for(unsigned int i = 0; i < instructions.size(); ++i){
				_situationTape->setCurrentNode(currentSituation);
				instruction = instructions.at(i);
				//update the situation tape
				updateSituationTape(instruction, situation.at(0), situation.at(1), situation.at(2));
			}
			if(_tempTape.size() == 0){
				Tape t =  Tape();
				_situationTape = std::make_shared<Tape>( t);
			}else{

				Tape t =  Tape(_tempTape);
				_situationTape = std::make_shared<Tape>( t);
			}
		}
		else{
			while((_situationTape->getData() != "00") && (_situationTape->getData() != "B")){
				_situationTape->nextNode();
			}
			_situationTape->nextNode();
			if(_situationTape->getData() == "B"){
				_accept = false;
				break;
			}
		}
		if((_acceptSates.find(situation.at(0)) != _acceptSates.end()) && (situation.at(1) == _epsilon) && (_startStackSymbol == situation.at(2)) ){
			_accept = true;
			break;
		}else{
			if(_situationTape->getData() == "B"){
				_accept = false;
				break;
			}
		}
		//clear all the temp data
		_tempTape.clear();
		instructions.clear();
		instruction.clear();
		situation.clear();
		topInput.str(std::string());
		topStack.str(std::string());
	}
	_verbose->closeCurrentFunction();

}

/**
 * @brief a helper function to print the situation tape
 * @pre object must be properly initialized
 */
void UniversalTuring::print(){
	_verbose->addFunction("UniversalTuring::print()");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::shared_ptr<NODE> temp = _situationTape->getCurrentNode();
	_situationTape->firstNode();
	//as long as we are not at the end
	while(_situationTape->getData() != "B"){
		//if 0 than it is a seperator in the transition
		//if 00 than a seperator between transitions
		//else just print the data
		if (_situationTape->getData() == "0"){
			std::cout<<" ";
		}else if(_situationTape->getData() == "00"){
			std::cout<<"---";
		}else{
			std::cout<<_converter.decodeSymbol(_situationTape->getData());
		}
		_situationTape->nextNode();
	}
	std::cout<<"\n";
	_situationTape->setCurrentNode(temp);
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to find the correct instruction for the current situation
 * @ return the instruction
 */
std::vector<std::string> UniversalTuring::findTMInstruction(){
	_verbose->addFunction("UniversalTuring::findTMInstruction()");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::vector<std::string> instruction;
	_programTape->firstNode();
	//as long as we have instructions
	while(_programTape->getData() != "B"){
		//get the current instruction
		while((_programTape->getData() != "00") && (_programTape->getData() != "B")){
			if(_programTape->getData() != "0")
				instruction.push_back(_programTape->getData());
			_programTape->nextNode();
		}
		//if we need to wright a "B" than we need to encode this
		if (_dataTape->getData() == "B")
			_dataTape->setData(_converter.encodeSymbol(_dataTape->getData()));
		//check if we need this instruction if yes than return else we go to the next
		if((instruction.at(0) == _stateTape->getData()) && (instruction.at(1) == _dataTape->getData())){
			_verbose->closeCurrentFunction();
			return instruction;
		}
		_programTape->nextNode();
		instruction.clear();
	}
	_verbose->closeCurrentFunction();
	return std::vector<std::string>();
}

/**
 * @brief a function to find the correct instruction for the current situation
 * @param string currentState the currentstate we are in
 * @param string topStack the top of the rest of the stack
 * @param string topInput the top of the input
 * @return all the posible instructions we can have in this situation.
 * @pre object must be properly initialized
 */
std::vector<std::vector<std::string> > UniversalTuring::findPDAInstruction(std::string currentState, std::string topStack, std::string topInput){
	_verbose->addFunction("UniversalTuring::findPDAInstruction(std::string currentState, std::string topStack, std::string topInput)");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::vector<std::string> instruction;
	std::vector<std::vector<std::string> > instructions;
	_programTape->firstNode();
	std::string lTop;
	//as long as we habe instructions
	while(_programTape->getData() != "B"){
		//get the current instruction
		while((_programTape->getData() != "00") && (_programTape->getData() != "B")){
			if(_programTape->getData() != "0"){
				instruction.push_back(_programTape->getData());
			}
			_programTape->nextNode();
		}
		//top of the stack
		lTop = top(instruction.at(2));
		//if this is one of the right instructions add this to the collection
		if((instruction.at(0) == currentState) && (instruction.at(1) == topInput ||  instruction.at(1) == _epsilon) && (lTop == topStack )){
			instructions.push_back(instruction);
		}
		_programTape->nextNode();
		instruction.clear();
	}

	_verbose->closeCurrentFunction();
	//return all the instructions that are found
	return instructions;
}

/**
 * @brief a helper function to easily update the situationTape
 * @param vector<string> instruction the instruction for the current update
 * @param string currentState the current state
 * @param string input the current input
 * @param string stack the current stack
 * @pre object must be properly initialized
 */
void UniversalTuring::updateSituationTape(std::vector<std::string>& instruction, std::string _currentState, std::string _input, std::string _stack){
	_verbose->addFunction("UniversalTuring::updateSituationTape(std::vector<std::string>& instruction, std::string _currentState, std::string _input, std::string _stack)");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::string currentState = std::string();
	std::string input = std::string();
	std::string stack = std::string();
	int index = 0;
	//the current state becomes the state of the result of the transition
	currentState = instruction.at(3);
	//if stack result variable of the transition has a size of 2 than we need to push on the stack
	if(instruction.at(4) == _epsilon){
		stack = _stack;
		if(stack.find_first_of("000") == std::string::npos){
			stack = pop(stack);
			stack = push(stack, _startStackSymbol, "000");
		}else if(top(stack) == _startStackSymbol){

		}else{
			stack = pop(stack);
		}

	}else{
		std::string newStack = instruction.at(4);
		stack = _stack;
		stack = pop(stack);
		stack = push(stack, newStack, "000");
	}
	//if the possible input is epsilon than we can go whit out notice to the next state, the input stays the same
	//else we need to pop the input, if the input becomes empty we add the epsilon symbol
	if(instruction.at(1) == _epsilon){
		input = _input;
	}else{
		input = _input;
		if(input.find_first_of("000") == std::string::npos){
			input = pop(input);
			input = push(input, _epsilon, "000");
		}
		else if(top(input) == _epsilon){

		}else{
			input = pop(input);
		}
	}
	//if the situation is not equal to its parent we need to mark as a new situation
	if((_currentState != currentState) || (_input != input) || (_stack != stack)){;
		if(stack.size() < _maxStackSize){
			//make an temp date for all the data
			while(_situationTape->getData() != "B"){
				_tempTape.push_back(_situationTape->getData());
				_situationTape->nextNode();
			}

			if(!_tempTape.empty()){
				if((*_tempTape.begin()) == "00")
					_tempTape.erase(_tempTape.begin());
				_tempTape.push_back("00");
			}
			_tempTape.push_back(currentState);
			_tempTape.push_back("0");
			_tempTape.push_back(input);
			_tempTape.push_back("0");
			_tempTape.push_back(stack);
		}
	}
	_verbose->closeCurrentFunction();
}

/**
 * @brief a function to 'push' something on a string
 * @param string stack the string we use as a stack
 * @param string input the input that should pushed on the string
 * @param string seperator the seperator that should be used between the symbols
 * @return the new stack(string)
 * @pre object must be properly initialized
 */
std::string UniversalTuring::push(std::string stack, std::string input, std::string seperator){
	_verbose->addFunction("UniversalTuring::push(std::string stack, std::string input, std::string seperator)");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	bool empty = false;
	if(stack.empty())
		empty = true;
	//insert the new input at the front
	stack.insert(0, input);
	//if the input was not empty we need to add a seperator
	if(!empty){
		stack.insert(input.size(), seperator);
	}
	_verbose->closeCurrentFunction();
	return stack;
}

/**
 * @brief a function to 'pop' something from a string
 * @param  string input the string that should be popped
 * @return the new string without the popped symbol
 * @pre object must be properly initialized
 */
std::string UniversalTuring::pop(std::string input){
	_verbose->addFunction("UniversalTuring::pop(std::string input)");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::vector<std::string> splitString;
	std::stringstream ss;
	ss.str(std::string());
	//split the string on "000" and than we skip the first one and the rest we retutn
	if(input.find_first_of("000") != std::string::npos){
		splitString = split(input, "000");
		for(unsigned int i = 1; i < splitString.size(); ++i){
			ss<<splitString.at(i);
			if(i < (splitString.size() - 1))
				ss<<"000";
		}
	}else{
		ss <<"";
	}
	_verbose->closeCurrentFunction();
	return ss.str();
}



/**
 * @brief a function to split a string in pieces with a given delimiter
 * @param string s the string that should be splitted.
 * @param string delim delimiter that should be used to split
 * @return a vector of the string split in pieces
 * @pre object must be properly initialized
 */
std::vector<std::string> UniversalTuring::split(const std::string &s, std::string delim) {
	_verbose->addFunction("UniversalTuring::split(const std::string &s, std::string delim)");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	bool accept = false;
	std::vector<std::string> result;
	if (delim.empty()) {
		result.push_back(s);
		_verbose->closeCurrentFunction();
		return result;
	}
	std::string::const_iterator substart = s.begin(), subend;
	while (!accept) {
		subend = search(substart, s.end(), delim.begin(), delim.end());
		std::string temp(substart, subend);
		if (!temp.empty()) {
			result.push_back(temp);
		}
		if (subend == s.end()) {
			accept = true;
		}
		substart = subend + delim.size();
	}
	_verbose->closeCurrentFunction();
	return result;
}

/**
 * @brief a function to get the top of a string
 * @param string input the string where we want the top of
 * @return the top of the string
 * @pre object must be properly initialized
 */
std::string UniversalTuring::top(std::string input){
	_verbose->addFunction("UniversalTuring::top(std::string input)");
	REQUIRE(this->properlyInitialized(), "UTM is not properly initialized");
	std::vector<std::string> splitString;
	if(input.find_first_of("000") != std::string::npos){
		splitString = split(input, "000");
		_verbose->closeCurrentFunction();
		return splitString.at(0);
	}else{
		_verbose->closeCurrentFunction();
		return input;
	}
}

} /* namespace mnb */
