/**
 * @file UniversalTuring.h
 * @class mnb::UniversalTuring
 * @brief class for UTM simulation
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Jan 13, 2014
 */

#pragma once
#ifndef UNIVERSALTURNING_H_
#define UNIVERSALTURNING_H_

#include "Tape.h"
#include <string>
#include <sstream>
#include <iostream>
#include "Turing.h"
#include <iterator>
#include <algorithm>
#include "../PDA/PDA.h"
#include <stack>
#include "../TupleTriple/Triple.h"
#include <set>
#include "../DesignByContract.h"
#include "../Verbose/Verbose.h"
#include "ConvertToUniversalInput.h"

namespace mnb {

class UniversalTuring {
public:

	UniversalTuring(PDA pda, std::string input);


	UniversalTuring(Turing tm);


	virtual ~UniversalTuring();

	void simulate();

	bool properlyInitialized();



private:


	void simulateTM();


	void simulatePDA();


	void print();


	std::vector<std::string> findTMInstruction();


	std::vector<std::vector<std::string> > findPDAInstruction(std::string currentState, std::string topStack, std::string topInput);


	void updateSituationTape(std::vector<std::string>& instruction, std::string _currentState, std::string _input, std::string _stack);



	std::string push(std::string stack, std::string input, std::string seperator);


	std::string pop(std::string input);


	std::string top(std::string input);


	std::vector<std::string> split(const std::string &s, std::string delim );


	std::shared_ptr<Tape> _programTape; //the tape with all the transitions
	std::shared_ptr<Tape> _dataTape;	//the tape with the input
	std::shared_ptr<Tape> _stateTape;	//the tape with all the states
	std::shared_ptr<Tape> _situationTape; //this we only need for the pda simulation.


	std::string _beginState;	//the beginstate of the automata
	std::set<std::string> _acceptSates; //the set of accepting states of the automata
	std::string _startStackSymbol; //this we only need for the pda simulation.
	std::string _epsilon; //this we only need for the pda simulation.

	std::string _currentState; //temp data for the pda simulation
	std::string _currentData; //temp data for the pda simulation

	std::vector<std::string> _tempTape; //temp data for the pda simulation

	int _maxStackSize; //temp data for the pda simulation

	bool _accept;
	Verbose* _verbose;

	const bool _simulateTM;

	UniversalTuring* InitCheck;

	ConvertToUniversalInput _converter; //a converter we can use everywere to decode


};

} /* namespace mnb */

#endif /* UNIVERSALTURNING_H_ */
