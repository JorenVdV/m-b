/*
 * Triple.h
 *
 *  Created on: 3 dec. 2013
 *      Author: bjorn
 */

#ifndef TRIPLE_H_
#define TRIPLE_H_

#include <sstream>

namespace mnb {

template <class T>
class Triple {
public:
	Triple();
	Triple(T first, T second, T thirth);
	virtual ~Triple();
	const T first() const;
	const T second() const;
	const T thirth() const;
	void add(T value);
	std::string toString();
	void clear();

	bool operator<(const Triple<T> & that){
		return ((_first < that.first()) && (_second < that.second()) && (_thirth < that.thirth()));
	}

private:
	T _first;
	T _second;
	T _thirth;
	int _counter;
};

template <class T>
std::ostream& operator<<(std::ostream& out, const Triple<T>& value){
	out<<"("<<value.first()<<","<<value.second()<<","<<value.thirth()<<")";
	return out;
}

template <class T>
Triple<T>::Triple(){
	_counter = 1;
}

template <class T>
Triple<T>::Triple(T first, T second, T thirth): _first(first), _second(second), _thirth(thirth) {
	_counter = 1;
}

template <class T>
Triple<T>::~Triple() {
	// TODO Auto-generated destructor stub
}

template <class T>
const T Triple<T>::first() const {
	return _first;
}

template <class T>
const T Triple<T>::second() const{
	return _second;
}

template <class T>
const T Triple<T>::thirth() const{
	return _thirth;
}

template <class T>
void Triple<T>::add(T value){
	if(_counter == 1){
		_first = value;
		_counter++;
	}
	else if(_counter == 2){
		_second = value;
		_counter++;
	}
	else if (_counter == 3){
		_thirth =  value;
		_counter = 1;
	}
}

template <class T>
std::string Triple<T>::toString(){
	std::stringstream ss;
	ss<<"("<<_first<<","<<_second<<","<<_thirth<<")";
	return ss.str();
}

template <class T>
void Triple<T>::clear(){
	_counter = 1;
}



} /* namespace mnb */
#endif /* TRIPLE_H_ */
