/*
 * Tuple.h
 *
 *  Created on: 3 dec. 2013
 *      Author: bjorn
 */

#ifndef TUPLE_H_
#define TUPLE_H_

#include <sstream>

namespace mnb {

template <class T>
class Tuple {
public:
	Tuple();
	Tuple(T first, T second);
	virtual ~Tuple();
	const T first() const;
	const T second() const;
	void add(T value);
	std::string toString();
	void clear();
	bool operator<(const Tuple<T> & that){
		return ((_first < that.first()) && (_second < that.second()));
	}



private:
	T _first;
	T _second;
	int _counter;
};

template <class T>
std::ostream& operator<<(std::ostream& out, const Tuple<T>& value){
	out<<"("<<value.first()<<","<<value.second()<<")";
	return out;
}

template <class T>
Tuple<T>::Tuple(){
	_counter = 1;
}

template <class T>
Tuple<T>::Tuple(T first, T second): _first(first), _second(second){
	_counter = 1;
}

template <class T>
Tuple<T>::~Tuple() {
	// TODO Auto-generated destructor stub
}

template <class T>
const T Tuple<T>::first() const {
	return _first;
}

template <class T>
const T Tuple<T>::second() const{
	return _second;
}


template <class T>
void Tuple<T>::add(T value){
	if(_counter == 1){
		_first = value;
		_counter++;
	}
	else if(_counter == 2){
		_second = value;
		_counter = 1;
	}
}

template <class T>
std::string Tuple<T>::toString(){
	std::stringstream ss;
	ss<<"("<<_first<<","<<_second<<")";
	return ss.str();
}

template <class T>
void Tuple<T>::clear(){
	_counter = 1;
}


} /* namespace mnb */
#endif /* TUPLE_H_ */
