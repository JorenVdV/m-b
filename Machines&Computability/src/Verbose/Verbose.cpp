/*
 * Verbose.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: EduardusdeMul
 */

#include "Verbose.h"

namespace mnb {

Verbose* Verbose::_instance = NULL;

/**
 * @brief a function to  initialize the verbose object, this makes sure that we use just one verbose object
 * @param bool verbose, should verbose be on of off
 * @post should be properly initialized
 */
Verbose* Verbose::instance(bool verbose){
	if(_instance == NULL){
		_instance = new Verbose(verbose);
	}
	return _instance;
}

/**
 * @brief the standard destructor for the verbose class
 */
Verbose::~Verbose() {
	// TODO Auto-generated destructor stub
}

/**
 * @brief add's a function to the output, and sets the indentation Level one higher
 * @param string functionName the name of the function with parameters.
 * @pre the functionName may not be empty
 * @post the indentation level should be increased
 */
void Verbose::addFunction(std::string functionName){
	REQUIRE(!functionName.empty(), "FunctionName may not be empty");
	int t_indentaionLevel = _indentationLevel;
	_currentFuction.push(std::make_pair(functionName, _indentationLevel));
	_message.append("Begin function: ");
	_message.append(functionName);
	_startFunction = true;
	this->print();
	_indentationLevel++;
	ENSURE(_indentationLevel > t_indentaionLevel, "the indentation level is not increased, while a new function is created");
}

/**
 * @brief a functions that prints output that the current function is finished, and sets the indentation Level to the previous level.
 * @post the indentation level should be decreased
 */
void Verbose::closeCurrentFunction(){
	int t_indentaionLevel = _indentationLevel;
	_indentationLevel = _currentFuction.top().second;
	_message.append("ending function: ");
	_message.append(_currentFuction.top().first);
	_currentFuction.pop();
	_endFunction = true;
	this->print();
	ENSURE(_indentationLevel < t_indentaionLevel, "the indentation level is not decreased, while a function is ended");
}

/**
 * @brief a function to put a random message to the verbose stream
 * @param string messages the message that should be added to the stream.
 * @pre the messages may not be empty
 */
void Verbose::comment(std::string message){
	//message.erase(remove_if(message.begin(), message.end(), isspace), message.end());
	//REQUIRE(!message.empty(), "the messages for the commit may not be empty");
	_message.append(message);
	this->print();
}

/**
 * @brief a function to put a random error message to the verbose stream
 * @param const char* err the error message that should be added to the stream.
 * @pre the messages may not be empty
 */
void Verbose::error(const char* err){
	//	REQUIRE(err != NULL, "the error message for the commit may not be empty");
	_message.append("Something went wrong: ");
	_message.append(err);
	_error = true;
	this->print();
}

/**
 * @brief a function to put a random error message to the verbose stream
 * @param std::string err the error message that should be added to the stream.
 * @pre the messages may not be empty
 */
void Verbose::error(std::string err){
	REQUIRE(!err.empty(), "the error message for the commit may not be empty");
	_message.append("Something went wrong: ");
	_message.append(err);
	_error = true;
	this->print();
}

/**
 * @brief a function to checks if the mode is verbose or not
 * @return true if verbose, false if not
 */
bool Verbose::isVerbose() const{
	return _verbose;
}

/**
 * @brief the constructor for the verbose class, to create a verbose object.
 * @param bool verbose, if true than there should be output, otherwise no output
 * @post the object should be properly initialized
 */
Verbose::Verbose(bool verbose) :_startFunction(false), _endFunction(false), _verbose(verbose), _indentationLevel(0), _message(""), _error(false) {
}

/**
 * @brief function to create the right indentation
 * @pre the message that we have to make can not be empty
 */
void Verbose::generatingIndentation(){
	//REQUIRE(!_message.empty(), "the message that we have to make can not be empty");
	std::string str;
	if(!_error){
		for(int i = 0; i < _indentationLevel ; i++ ){
			if(_startFunction){
				str.append(">>");
			}else if(_endFunction){
				str.append("<<");
			}else{
				str.append("--");
			}
		}
	}
	else{
		for(int i = 0; i < _indentationLevel; i++ ){
			str.append("!!");
		}
	}
	str.append(_message);
	_message = str;
	str.clear();
	_startFunction = false;
	_endFunction = false;
}

/**
 * @brief function to print all the messages
 * @pre the message that we have to make can not be empty
 */
void Verbose::print(){
	//REQUIRE(!_message.empty(), "the message that we have to make can not be empty");
	if(_verbose){
		generatingIndentation();
		if(!_error){
			std::cout<<zkr::cc::fore::cyan<<_message<<std::endl;
		}else{
			std::cout<<zkr::cc::fore::lightred<<_message<<std::endl;
		}
	}
	std::cout<<zkr::cc::fore::console;
	_error = false;
	_message.clear();
}

} /* namespace mnb */
