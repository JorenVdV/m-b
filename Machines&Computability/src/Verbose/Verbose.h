/*
 * Verbose.h
 *
 *  Created on: Nov 19, 2013
 *      Author: EduardusdeMul
 */

#pragma once
#ifndef VERBOSE_H_
#define VERBOSE__H_

#include <stack>
#include <string>
#include <iostream>
#include <sstream>
#include <utility>
#include <algorithm>
#include <stdlib.h>
#include "../DesignByContract.h"
#include "../Color/ccolor.h"

namespace mnb {

class Verbose {
public:


	static Verbose* instance(bool verbose);


	virtual ~Verbose();


	void addFunction(std::string functionName);


	void closeCurrentFunction();


	void comment(std::string message);


	void error(const char* err);


	void error(std::string err);

	/*
	 * the overloaded function for the post increment
	 * @param int the base value
	 * @post the indentation level should be increased
	 */
	Verbose operator++(int){
		Verbose original = *this;
		int t_indentaionLevel = _indentationLevel;
		_indentationLevel++;
		ENSURE(_indentationLevel > t_indentaionLevel, "the indentation level is not increased");
		return original;

	}

	/*
	 * the overloaded function for the post decrement
	 * @param  int the base value
	 * @post the indentation level should be decreased
	 */
	Verbose operator--(int){
		Verbose original = *this;
		int t_indentaionLevel = _indentationLevel;
		_indentationLevel--;
		ENSURE(_indentationLevel < t_indentaionLevel, "the indentation level is not decreased");
		return original;
	}

	bool isVerbose() const;


private:


		Verbose(bool verbose);


	void  generatingIndentation();


	void print();


	static Verbose* _instance;
	bool _startFunction;
	bool _endFunction;
	bool _verbose;
	std::stack< std::pair<std::string, int> > _currentFuction;
	int _indentationLevel;

	std::string  _message;
	bool _error;

};



} /* namespace mnb */
#endif /* VERBOSE__H_ */
