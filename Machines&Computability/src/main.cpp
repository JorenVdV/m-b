/*
 * @file main.cpp
 *
 * @author Joren Van de Vondel & Eduardus de Mul
 *  Created on: Nov 15, 2013
 */


#include <iostream>

#include "CFG/CFG.h"
#include "CONV/CONV.h"
#include "PDA/PDA.h"
#include "TURING/UniversalTuring.h"
#include "TURING/Turing.h"
#include "TURING/Tape.h"
#include <set>
#include <vector>
#include <iostream>
#include <dirent.h>
#include <sstream>
#include <string>
#include "../TupleTriple/Triple.h"
#include "../TupleTriple/Tuple.h"
#include "Verbose/Verbose.h"
#include "Parser/tinyxml2.h"
#include<dirent.h>

std::vector<std::string> results;

int chooseIntInput();
std::string chooseStringInput();
void search(std::string curr_directory, std::string extension);

/**
 * @brief Main function calls up the program
 *
 * Ask for a directory with xml files for wich he wil prompt possible actions.
 * Actions will be possible to execute in main.
 *
 * @return Exit code
 */
int main(int argc, char* argv[]){
	mnb::Verbose* verb = verb->instance(false);
	int choice = 0;
	std::string input;
	std::string folder;
	//check which folder the xml are located (a USB mabey)
	std::cout<<"wich folder do you want to use?[if no input then the default folder: ../XMLInput]\n";
	getline(std::cin, folder);
	if(folder.empty())
		folder = "../XMLInput";
	//search for xml files
	search(folder, "xml");
	// print the xml files
	if(results.size() != 0){
		std::cout<<"Found the following files:"<<std::endl;
		for(unsigned int i = 0; i < results.size(); ++i ){
			std::cout<<"["<<i<<"]"<<results.at(i)<<std::endl;
		}
		//get the file we want to use
		std::cout<<"which do you want to use? (select the number)"<<std::endl;
		choice = chooseIntInput();

		//check what kind of automata is is.
		tinyxml2::XMLDocument doc;
		std::string fileName;
		if((*folder.end()) != '/')
			fileName = folder.append("/").append(results.at(choice));
		else
			fileName = folder.append(results.at(choice));
		doc.LoadFile(fileName.c_str());
		//temp data for correct verbose output
		std::string elementName;

		tinyxml2::XMLNode *rootNode = doc.FirstChild();
		if(!rootNode) throw "There was no root node found.";

		//search the sibling after the root
		tinyxml2::XMLNode* siblingNode = rootNode ->NextSibling();
		if(!siblingNode) throw "there is no siblingNode for the rootNode";
		elementName = std::string(siblingNode->Value());

		choice = -1;
		//give the possible functions per automata
		if(elementName == "PDA"){
			mnb::PDA pda(fileName, false);
			while(choice != 8){
				std::cout<<"You can perform the follow operations: "<<std::endl;
				std::cout<<"[0] check if the pda is a valid pda"<<std::endl;
				std::cout<<"[1] print the pda to screen"<<std::endl;
				std::cout<<"[2] print the pda to .xml"<<std::endl;
				std::cout<<"[3] print the pda to .dot"<<std::endl;
				std::cout<<"[4] print the transition table"<<std::endl;
				std::cout<<"[5] simulate the pda"<<std::endl;
				std::cout<<"[6] convert pda to cfg"<<std::endl;
				std::cout<<"[7] simulate the pda with a UTM"<<std::endl;
				std::cout<<"[8] exit"<<std::endl;
				std::cout<<"which do you want to execute? (select the number)"<<std::endl;
				choice = chooseIntInput();
				if(choice == 0){
					if(pda.isPDA() == true)
						std::cout<<"The pda is valid"<<std::endl;
					else
						std::cout<<"The pda is not valid"<<std::endl;
				}else if(choice == 1){
					pda.print();
				}else if(choice == 2){
					pda.printToXML("outputPDA");
					std::cout<<"the pda is saved to a .xml file under the name outputPDA.xml"<<std::endl;
				}else if(choice == 3){
					pda.printToDOT();
					std::cout<<"the pda is saved to a .dot file under the name PDA.dot"<<std::endl;
				}else if(choice == 4){
					pda.transitionToTable();
				}else if(choice == 5){
					std::cout<<"Give a input for the pda to test"<<std::endl;
					input = chooseStringInput();
					bool boolean = pda.simulate(input);
					std::cout<<"There is a file created with all the ID's under the name ID.txt"<<std::endl;
				}else if(choice == 6){
					mnb::CONV convert(false);
					mnb::CFG cfg = convert.PDAtoCFG(pda);
					cfg.print();
					cfg.printToXML("temp_CGF.xml");
					std::cout<<"A xml file is created for later use, under the name: temp_CFG.xml"<<std::endl;

				}else if(choice == 7){
					std::cout<<"Give a input for the UTM to test"<<std::endl;
					input = chooseStringInput();
					mnb::UniversalTuring UTM(pda, input);
					UTM.simulate();
				}else if(choice == 8){
					std::cout<<"Exit"<<std::endl;
				}else{
					std::cout<<"there is no function with this number"<<std::endl;
				}
			}
		}else if (elementName == "CFG"){
			mnb::CFG cfg(fileName, false);
			while(choice != 6){
				std::cout<<"You can perform the follow operations: "<<std::endl;
				std::cout<<"[0] check if the cfg is a valid cfg"<<std::endl;
				std::cout<<"[1] print the cfg to .xml"<<std::endl;
				std::cout<<"[2] print the cfg to screen"<<std::endl;
				std::cout<<"[3] check if a string is in the grammar"<<std::endl;
				std::cout<<"[4] set the cfg in chomsky()"<<std::endl;
				std::cout<<"[5] convert cfg to pda"<<std::endl;
				std::cout<<"[6] exit"<<std::endl;
				std::cout<<"which do you want to execute? (select the number)"<<std::endl;
				choice = chooseIntInput();
				if(choice == 0){
					if(cfg.isCFG() == true)
						std::cout<<"cfg is valid"<<std::endl;
					else
						std::cout<<"cfg is not valid"<<std::endl;
				}else if(choice == 1){
					cfg.printToXML("outputCFG.xml");
					std::cout<<"the cfg is saved to .xml file under the name outputCFG.xml"<<std::endl;
				}else if(choice == 2){
					cfg.print();
				}else if(choice == 3){
					std::cout<<"Give a input for the cfg to test"<<std::endl;
					input = chooseStringInput();
					mnb::CFG tcfg = cfg;
					cfg.Chomsky();
					bool boolean = cfg.stringInGrammar(input);
					cfg = tcfg;
				}else if(choice == 4){
					cfg.Chomsky();
					std::cout<<"the cfg is set in chomsky:"<<std::endl;
					cfg.print();
				}else if(choice == 5){
					mnb::CONV convert(false);
					mnb::PDA pda = convert.CFGtoPDA(cfg);
					pda.print();
					pda.printToXML("temp_PDA");
					std::cout<<"A xml file is created for later use, under the name: temp_PDA.xml"<<std::endl;
				}else if(choice == 6){
					std::cout<<"Exit"<<std::endl;
				}else{
					std::cout<<"there is no function with this number"<<std::endl;
				}
			}
		}else if(elementName == "Turing" ){
			mnb::Turing tm(fileName);
			while(choice != 3){
				std::cout<<"You can perform the follow operations: "<<std::endl;
				std::cout<<"[0] print the TM to the screen"<<std::endl;
				std::cout<<"[1] simulate the TM"<<std::endl;
				std::cout<<"[2] simulate the TM with UTM "<<std::endl;
				std::cout<<"[3] Exit"<<std::endl;
				choice = chooseIntInput();
				if(choice == 0){
					tm.print();
				}else if(choice == 1){
					std::cout<<"Give a input for the UTM to test"<<std::endl;
					input = chooseStringInput();
					std::vector<std::string> data;
					std::stringstream ss;
					for(auto it = input.begin(); it != input.end(); ++it ){
						ss << (*it);
						data.push_back(ss.str());
						ss.str(std::string());
					}
					mnb::Tape t = mnb::Tape(data);
					tm.setT(t);
					tm.simulate();
					std::cout<<"data Tape :";
					t.print();
				}else if(choice == 2){
					std::cout<<"Give a input for the UTM to test"<<std::endl;
					input = chooseStringInput();
					std::vector<std::string> data;
					std::stringstream ss;
					for(auto it = input.begin(); it != input.end(); ++it ){
						ss << (*it);
						data.push_back(ss.str());
						ss.str(std::string());
					}
					mnb::Tape t = mnb::Tape(data);
					tm.setT(t);
					mnb::UniversalTuring utm(tm);
					utm.simulate();
				}else if(choice == 3){
					std::cout<<"Exit"<<std::endl;
				}else{
					std::cout<<"there is no function with this number"<<std::endl;

				}

			}

		}else{
			std::cout<<"No valid automata in the xml file";
		}

	}else{
		std::cout<<"No valid folder"<<std::endl;
	}

	return 0;
}

int chooseIntInput(){
	int choice = 0;
	while(true){
		std::cin >> choice;
		if((choice < 0) || ((int)choice != choice))
			std::cout<<"Please give a correct input"<<std::endl;
		else
			return choice;
	}
	return 0;
}

std::string chooseStringInput(){
	std::string input = std::string();
	while(true){
		getline(std::cin, input);
		if(input.empty())
			std::cout<<"please give some input"<<std::endl;
		else
			return input;
	}
	return std::string();
}

void search(std::string curr_directory, std::string extension){
	DIR* dir_point = opendir(curr_directory.c_str());
	if(dir_point){
		dirent* entry = readdir(dir_point);
		while (entry){                                                                        // if !entry then end of directory
			if (entry->d_type == DT_DIR){                                				  // if entry is a directory
				std::string fname = entry->d_name;
				if (fname != "." && fname != "..")
					search(entry->d_name, extension);        					  // search through it
			}
			else if (entry->d_type == DT_REG){                							  // if entry is a regular file
				std::string fname = entry->d_name;        							  // filename
				// if filename's last characters are extension
				if (fname.find(extension, (fname.length() - extension.length())) != std::string::npos)
					results.push_back(fname);                // add filename to results vector
			}
			entry = readdir(dir_point);
		}
	}
}
